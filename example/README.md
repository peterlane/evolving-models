# Examples

The jar file 'fivefourexperiment' contains implementation of the data 
and models for the 5-4 experiment, and a gui application to experiment 
with the algorithms and datasets.  The jar file can be run (by double-clicking).

Java files in this 'examples' folder use the library and models in 
'fivefourexperiment' to illustrate direct use of the library as simple 
command-line programs to locate and retrieve optimised model sets.
To compile, you must include both the library and fivefourexperiment.jar 
on the classpath.
