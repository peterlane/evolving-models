package fivefourexperiment;

import java.util.List;

public class InstanceCategoryPair {

  public InstanceCategoryPair (List<Instance> instances, String category) {
    _instances = instances;
    _category = category;
  }

  public List<Instance> instances () {
    return _instances;
  }

  public String category () {
    return _category;
  }

  private final List<Instance> _instances;
  private final String _category;
}
