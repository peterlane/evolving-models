package fivefourexperiment;

import java.util.ArrayList;
import java.util.List;

public class AadConstraint extends SimpleConstraint {

	public AadConstraint (String name, double[] targets) {
    super (name, targets);
	}

	public double computeMatch (List<Double> results) {
		if (results.isEmpty ()) return 0.0;

		double totalAbsoluteDifference = 0.0;

		for (int i = 0, n = Math.min(results.size (), targets.length); i < n; i+=1) {
			if (results.get (i) == null) {
				; // ignore this error
			} else {
				totalAbsoluteDifference += 
					Math.abs (results.get (i) - targets[i]);
			}
		}

		return totalAbsoluteDifference / results.size ();
	}

	public String toString () {
		return "AAD " + name;
	}
}

