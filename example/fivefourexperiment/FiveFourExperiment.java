package fivefourexperiment;

import java.util.ArrayList;
import java.util.List;

import fivefourexperiment.AadConstraint;
import fivefourexperiment.SseConstraint;
import fivefourexperiment.InstanceCategoryPair;
import fivefourexperiment.models.*;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/** 
 * Define the data-set and separation into training/tests sets for
 * the Five-Four experiment.
 */
public class FiveFourExperiment implements Experiment {
	public final List<InstanceCategoryPair> training; // make visible to models
	public final List<Instance> allInstances;  // make visible to models

	public FiveFourExperiment () {
		training = new ArrayList<InstanceCategoryPair> ();
		allInstances = new ArrayList<Instance> ();

    List<Instance> categoryA = new ArrayList<> ();
    categoryA.add (new Instance ("E1", true, true, true, false));
		categoryA.add (new Instance ("E2", true, false, true, false));
		categoryA.add (new Instance ("E3", true, false, true, true));
		categoryA.add (new Instance ("E4", true, true, false, true));
		categoryA.add (new Instance ("E5", false, true, true, true));
    addTraining (categoryA, "A");

    List<Instance> categoryB = new ArrayList<> ();
    categoryB.add (new Instance ("E6", true, true, false, false));
		categoryB.add (new Instance ("E7", false, true, true, false));
		categoryB.add (new Instance ("E8", false, false, false, true));
		categoryB.add (new Instance ("E9", false, false, false, false));
    addTraining (categoryB, "B");

		addTestItem (new Instance ("E10", true, false, false, true));
		addTestItem (new Instance ("E11", true, false, false, false));
		addTestItem (new Instance ("E12", true, true, true, true));
		addTestItem (new Instance ("E13", false, false, true, false));
		addTestItem (new Instance ("E14", false, true, false, true));
		addTestItem (new Instance ("E15", false, false, true, true));
		addTestItem (new Instance ("E16", false, true, false, false));
	}

  /**
   * Optional method, allows gui to display the instances used in testing 
   * when displaying individual models.
   */
  public List<Instance> getTestInstances () {
    return allInstances;
  }
  
  private List<Constraint> constraints;

  /**
   * Required by Experiment interface: returns a list of the constraints 
   * available for this simulation experiment.
   */
  public List<Constraint> getConstraints () {
    if (constraints == null) {
      constraints = new ArrayList<Constraint> ();

      makeAadConstraints ();
      makeSseConstraints ();
    }
    return constraints;
  }

  /**
   * Required by Experiment interface: returns a list of the theories (model classes) 
   * to use in the current simulation experiment.
   */
  public List<Class<? extends Model>> getTheories () {
    List<Class<? extends Model>> theories = new ArrayList<> ();

    theories.add (ChrestModel.class);
    theories.add (ConnectionistModel.class);
    theories.add (ContextModel.class);
    theories.add (PrototypeModel.class);

    return theories;
  }

  /**
   * Required by Experiment interface: requests model to build/train itself
   * given its current set of parameters, and return a list of 
   * results against the test constraints.
   */
  public List<Double> evaluate (Model model) {
		if (model instanceof ChrestModel) {
			return ((ChrestModel)model).apply (this);
		} else if (model instanceof ConnectionistModel) {
			return ((ConnectionistModel)model).apply (this);
		} else if (model instanceof MathematicalModel) {
			return ((MathematicalModel)model).apply (this);
		}
		// error to reach this point
		return new ArrayList<Double> ();
  }

	// private methods to assist creation of datasets by automatically
	// adding item to total dataset

	private void addTraining (List<Instance> instances, String category) {
		training.add (new InstanceCategoryPair (instances, category));
    for (Instance instance : instances) {
  		allInstances.add (instance);
    }
	}

	private void addTestItem (Instance instance) {
		allInstances.add (instance);
	}

  /**
   * The target results, taken from Smith and Minda (2000).
   */
  private static final double[][] targets = new double[][] {
    { 0.78, 0.88, 0.81, 0.88, 0.81, 0.16, 0.16, 0.12,
      0.03, 0.59, 0.31, 0.94, 0.34, 0.50, 0.62, 0.16 },
      { 0.97, 0.97, 0.92, 0.81, 0.72, 0.33, 0.28, 0.03,
        0.05, 0.72, 0.56, 0.98, 0.23, 0.27, 0.39, 0.09 },
      { 0.89, 0.94, 0.94, 0.72, 0.78, 0.27, 0.30, 0.09, 
        0.05, 0.45, 0.20, 0.88, 0.58, 0.08, 0.75, 0.12 },
      { 0.77, 0.97, 0.98, 0.70, 0.60, 0.55, 0.28, 0.17, 
        0.13, 0.73, 0.65, 0.87, 0.22, 0.28, 0.52, 0.12 },
      { 0.81, 0.75, 0.95, 0.77, 0.80, 0.42, 0.30, 0.25, 
        0.11, 0.62, 0.31, 0.89, 0.34, 0.31, 0.62, 0.20 },
      { 0.69, 0.77, 0.92, 0.50, 0.77, 0.36, 0.48, 0.22, 
        0.09, 0.59, 0.41, 0.87, 0.49, 0.30, 0.57, 0.20 },
      { 0.66, 0.47, 0.56, 0.50, 0.31, 0.47, 0.47, 0.31, 
        0.41, 0.58, 0.55, 0.69, 0.41, 0.52, 0.50, 0.31 },
      { 0.68, 0.61, 0.74, 0.77, 0.35, 0.68, 0.35, 0.16, 
        0.36, 0.72, 0.66, 0.76, 0.16, 0.31, 0.35, 0.32 },
      { 0.73, 0.88, 0.95, 0.77, 0.73, 0.25, 0.20, 0.23, 
        0.06, 0.62, 0.50, 0.86, 0.34, 0.42, 0.59, 0.06 },
      { 0.95, 0.88, 0.98, 0.94, 0.92, 0.28, 0.23, 0.08,
        0.05, 0.61, 0.28, 0.98, 0.14, 0.36, 0.61, 0.09 },
      { 0.88, 0.80, 0.95, 0.81, 0.84, 0.31, 0.34, 0.16, 
        0.02, 0.75, 0.34, 0.94, 0.20, 0.42, 0.67, 0.06 },
      { 0.73, 0.84, 0.89, 0.75, 0.70, 0.25, 0.31, 0.19, 
        0.16, 0.55, 0.41, 0.80, 0.47, 0.39, 0.61, 0.22 },
      { 0.94, 0.75, 0.91, 0.91, 0.86, 0.31, 0.34, 0.09, 
        0.06, 0.50, 0.09, 0.92, 0.31, 0.55, 0.50, 0.16 },
      { 0.89, 0.73, 0.89, 0.73, 0.70, 0.30, 0.19, 0.17, 
        0.02, 0.67, 0.34, 0.92, 0.27, 0.42, 0.56, 0.05 },
      { 0.73, 0.84, 0.84, 0.81, 0.73, 0.22, 0.38, 0.22, 
        0.17, 0.42, 0.47, 0.77, 0.48, 0.52, 0.58, 0.34 },
      { 0.88, 0.67, 0.81, 0.86, 0.88, 0.34, 0.25, 0.09, 
        0.11, 0.50, 0.17, 0.95, 0.27, 0.44, 0.55, 0.14 },
      { 0.78, 0.60, 0.84, 0.75, 0.75, 0.21, 0.22, 0.28, 
        0.12, 0.75, 0.23, 0.86, 0.23, 0.34, 0.59, 0.17 },
      { 0.84, 0.92, 0.93, 0.91, 0.78, 0.13, 0.21, 0.08, 
        0.11, 0.64, 0.45, 0.83, 0.48, 0.56, 0.56, 0.18 },
      { 0.53, 0.78, 0.75, 0.82, 0.70, 0.35, 0.35, 0.20, 
        0.25, 0.62, 0.53, 0.71, 0.45, 0.75, 0.53, 0.23 },
      { 0.93, 0.90, 0.95, 0.85, 0.60, 0.05, 0.25, 0.07, 
        0.12, 0.65, 0.42, 0.82, 0.45, 0.40, 0.45, 0.17 },
      { 0.97, 1.00, 1.00, 0.95, 0.88, 0.18, 0.10, 0.03, 
        0.00, 0.62, 0.42, 0.90, 0.55, 0.50, 0.65, 0.20 },
      { 0.93, 1.00, 1.00, 1.00, 0.93, 0.00, 0.15, 0.00, 
        0.05, 0.65, 0.42, 0.90, 0.45, 0.60, 0.62, 0.12 },
      { 0.77, 0.78, 0.83, 0.64, 0.61, 0.39, 0.41, 0.21, 
        0.15, 0.56, 0.41, 0.82, 0.40, 0.32, 0.53, 0.20 },
      { 0.94, 1.00, 0.97, 0.98, 0.92, 0.13, 0.06, 0.02, 
        0.02, 0.94, 0.69, 0.94, 0.03, 0.14, 0.32, 0.08 },
      { 0.81, 0.84, 0.86, 0.70, 0.72, 0.32, 0.31, 0.20, 
        0.11, 0.63, 0.38, 0.85, 0.34, 0.32, 0.59, 0.19 },
      { 0.85, 0.76, 0.85, 0.62, 0.72, 0.50, 0.51, 0.27,
        0.21, 0.53, 0.46, 0.89, 0.37, 0.43, 0.57, 0.27 },
      { 0.85, 0.77, 0.94, 0.77, 0.72, 0.43, 0.33, 0.20,
        0.11, 0.58, 0.34, 0.99, 0.28, 0.37, 0.58, 0.17 },
      { 0.85, 0.72, 0.96, 0.86, 0.77, 0.35, 0.29, 0.21,
        0.08, 0.62, 0.24, 0.99, 0.29, 0.50, 0.59, 0.11 },
      { 0.90, 0.75, 0.97, 0.95, 0.90, 0.23, 0.20, 0.19, 
        0.04, 0.59, 0.23, 0.99, 0.33, 0.43, 0.60, 0.14 }
  };

  private void makeAadConstraints () {
    int i = 0;
		for (double[] results : targets) {
      i += 1;
			constraints.add (new AadConstraint ("" + i, results));
		}
		constraints.add (new AadConstraint ("AVG", getAverageTargets ()));
	}

	private void makeSseConstraints () {
    int i = 0;
		for (double[] results : targets) {
      i += 1;
			constraints.add (new SseConstraint ("" + i, results));
		}
		constraints.add (new SseConstraint ("AVG", getAverageTargets ()));
	}
	
	private double[] getAverageTargets () {
		int numExpts = targets[0].length;
		int numConstraints = targets.length;
		
		double[] averages = new double[numExpts];
		for (int i = 0; i < numExpts; ++i) {
			averages[i] = 0.0;
			for (int j = 0; j < numConstraints; ++j) {
				averages[i] += targets[j][i];
			}
			averages[i] /= numConstraints;
		}

		return averages;
	}
}

