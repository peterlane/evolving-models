package fivefourexperiment.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.Icon;

import fivefourexperiment.FiveFourExperiment;
import fivefourexperiment.Instance;
import fivefourexperiment.InstanceCategoryPair;
import info.peterlane.mdk.optimisation.Model;

/** Connectionist models are simple perceptrons.  The probability of responding with 
  * category A is obtained by training a population of models, and then extracting 
  * the number of individual models which classify an instance in category A.
  */
public class ConnectionistModel implements Model {
	private static int totalModels;
	private final int identifier;
	private static final Random r = new Random ();

	// basic parameters defining the model
	public double 
		theta, 	// threshold on output
		eta, 		// learning rate
		rho;		// probability of learning a pattern
	public int 
		cycles,	// number of training cycles
		populationSize; // number of inidividuals in population

	public static final int MAX_CYCLES = 1000; // maximum allowed number of training cycles
	public static final int MAX_POPULATION = 100; // maximum number of individuals to train

	public ConnectionistModel () {
		identifier = totalModels;
		totalModels += 1;
		theta = r.nextDouble ();
		eta = r.nextDouble ();
		rho = r.nextDouble ();
		cycles = r.nextInt (MAX_CYCLES) + 1; // between 1 and MAX_CYCLES inclusive
		populationSize = r.nextInt (MAX_POPULATION) + 1;
	}

	public double getTheta () { return theta; }
	public double getEta () { return eta; }
	public double getRho () { return rho; }
	public int getCycles () { return cycles; }
	public int getPopulationSize () { return populationSize; }
	public String toString () { return "Connectionist_" + identifier; }

	// keep a cache of results to save recomputing
	private List<Double> results;
	public List<Double> apply (FiveFourExperiment expt) {
		if (results == null) {
			results = new ArrayList<Double> ();

			// create a pool of models
			List<Perceptron> models = new ArrayList<> ();
			for (int i = 0; i < populationSize; ++i) {
				models.add (new Perceptron (theta, eta, rho));
			}
			// train them
			for (Perceptron model : models) {
				model.train (expt, cycles);
			}
			// get proportion saying A for each test instance
			for (Instance instance : expt.allInstances) {
				int numCategoryA = 0;
				for (Perceptron model : models) {
					if (model.inCategoryA (instance)) numCategoryA += 1;
				}

				results.add (((double)numCategoryA / (double)populationSize));
			}
		}
		return results;
	}

  public int getParameterCount () {
    return 5;
  }

	public String getParameterName (int parameter) {
		String[] names = { "Theta", "Eta", "Rho", "Learning cycles", "Population size" };
    return names[parameter];
	}

	public double getParameter (int parameter) {
		switch (parameter) {
			case 0: return getTheta ();
			case 1: return getEta ();
			case 2: return getRho ();
			case 3: return getCycles ();
			default: // case 4:
				return getPopulationSize ();
		}
	}

  public void setParameter (int parameter, double value) {
		switch (parameter) {
			case 0: theta = value; break;
			case 1: eta = value; break;
			case 2: rho = value; break;
			case 3: cycles = (int)value; break;
			default: // case 4:
							populationSize = (int)value; break;
		}
	}

	public void checkParameters () {
		if (theta < 0.0) theta = 0.0;
		if (theta > 1.0) theta = 1.0;
		if (eta < 0.0) eta = 0.0;
		if (eta > 1.0) eta = 1.0;
		if (rho < 0.0) rho = 0.0;
		if (rho > 1.0) rho = 1.0;
		if (cycles < 1) cycles = 1;
		if (cycles > MAX_CYCLES) cycles = MAX_CYCLES;
		if (populationSize < 1) populationSize = 1;
		if (populationSize > MAX_POPULATION) populationSize = MAX_POPULATION;
	}

  /**
   * Public static method provided to give icon for GUI.
   */
  public static Icon getIcon () {
    return new ImageIcon (ChrestModel.class.getResource("icons/ConnectionistModel.gif"));
  }
}

class Perceptron {
	private static final Random r = new Random ();
	private double theta, eta, rho;
	private double w0, w1, w2, w3, bias;

	public Perceptron (double theta, double eta, double rho) {
		this.theta = theta;
		this.eta = eta;
		this.rho = rho;
		// initialise weights to values in [-0.5, 0.5]
		w0 = r.nextDouble () - 0.5;
		w1 = r.nextDouble () - 0.5;
		w2 = r.nextDouble () - 0.5;
		w3 = r.nextDouble () - 0.5;
		bias = r.nextDouble () - 0.5;
	}

	public void train (FiveFourExperiment expt, int numCycles) {
		for (int i = 0; i < numCycles; ++i) {
			train (expt);
		}
	}

	// specialised to 5-4 task
	private void train (FiveFourExperiment expt) {
		for (InstanceCategoryPair icp : expt.training) {
			for (Instance instance : icp.instances ()) {
				trainInstance (instance, (icp.equals("A") ? 1 : -1));
			}
		}
	}

	private void trainInstance (Instance instance, int target) {
		if (r.nextDouble () > rho) return; // don't learn in (1 - rho) cases
		
		double delta = target - (inCategoryA (instance) ? 1 : -1);

		bias += delta * eta;
		w0 += delta * eta * instance.getA0AsInt ();
		w1 += delta * eta * instance.getA1AsInt ();
		w2 += delta * eta * instance.getA2AsInt ();
		w3 += delta * eta * instance.getA3AsInt ();
	}

	/** Given instance is in category A if the net value exceeds the threshold, theta. */
	public boolean inCategoryA (Instance instance) {
		return computeNetValue (instance) > theta;
	}

	private double computeNetValue (Instance instance) {
		return 	w0 * instance.getA0AsInt () +
			w1 * instance.getA1AsInt () +
			w2 * instance.getA2AsInt () +
			w3 * instance.getA3AsInt () +
			bias;
	}
}
