package fivefourexperiment.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.Icon;

import fivefourexperiment.FiveFourExperiment;
import fivefourexperiment.Instance;
import info.peterlane.mdk.optimisation.Model;

public abstract class MathematicalModel implements Model {
	static Random r = new Random ();
	final int identifier;

	double w0, w1, w2, w3, guessing, sensitivity;
	
	public double getW0 () { return w0; }
	public double getW1 () { return w1; }
	public double getW2 () { return w2; }
	public double getW3 () { return w3; }
	public double getGuessing () { return guessing; }
	public double getSensitivity () { return sensitivity; }
	
	MathematicalModel (int identifier) {
		this.identifier = identifier;
		w0 = r.nextDouble ();
		w1 = r.nextDouble ();
		w2 = r.nextDouble ();
		w3 = r.nextDouble ();
		guessing = r.nextDouble ();
		sensitivity = r.nextDouble ();
		checkParameters ();
	}

	double computeDistance (Instance i1, Instance i2) {
		return (sensitivity  * 
				((i1.sameA0 (i2) ? 0.0 : w0) +
				 (i1.sameA1 (i2) ? 0.0 : w1) +
				 (i1.sameA2 (i2) ? 0.0 : w2) +
				 (i1.sameA3 (i2) ? 0.0 : w3)));
	}

	public boolean equals (Object o) {
		if (!(o instanceof MathematicalModel)) return false;
		MathematicalModel model = (MathematicalModel)o;

		return (w0 == model.w0 &&
			w1 == model.w1 &&
			w2 == model.w2 &&
			w3 == model.w3 &&
			guessing == model.guessing &&
			sensitivity == model.sensitivity);
	}

	// ********** Model interface 
	abstract public List<Double> apply (FiveFourExperiment expt);

  public int getParameterCount () {
    return 6;
  }

	public String getParameterName (int parameter) {
		String[] names = { "Weight 0", "Weight 1", "Weight 2", "Weight 3",
			"Guessing", "Sensitivity" };
    return names[parameter];
	}

	public double getParameter (int parameter) {
		switch (parameter) {
			case 0: return getW0 ();
			case 1: return getW1 ();
			case 2: return getW2 ();
			case 3: return getW3 ();
			case 4: return getGuessing ();
			default: // case 5:
				return getSensitivity ();
		}
	}

  public void setParameter (int parameter, double value) {
		switch (parameter) {
			case 0: w0 = value; break;
			case 1: w1 = value; break;
			case 2: w2 = value; break;
			case 3: w3 = value; break;
			case 4: guessing = value; break;
			default: // case 5:
				sensitivity = value; break;
		}
	}

	public void checkParameters () {
		if (w0 < 0.0) w0 = -w0;
		if (w1 < 0.0) w1 = -w1;
		if (w2 < 0.0) w2 = -w2;
		if (w3 < 0.0) w3 = -w3;
		double sum = w0 + w1 + w2 + w3;
		if (sum != 0) {
			w0 = w0 / sum;
			w1 = w1 / sum;
			w2 = w2 / sum;
			w3 = w3 / sum;
		}
		if (sensitivity < 0.0) sensitivity = - sensitivity;
		if (guessing < 0.0) guessing = - guessing;
		if (guessing > 1.0) guessing = 1.0;
	}

  /**
   * Public static method provided to give icon for GUI.
   */
  public static Icon getIcon () {
    return new ImageIcon (ChrestModel.class.getResource("icons/MathModel.gif"));
  }
}

