package fivefourexperiment.models;

import java.util.ArrayList;
import java.util.List;

import fivefourexperiment.FiveFourExperiment;
import fivefourexperiment.Instance;
import fivefourexperiment.InstanceCategoryPair;
import info.peterlane.mdk.optimisation.Model;

public class ContextModel extends MathematicalModel {
	private static int totalModels;

	public ContextModel () {
		super (totalModels);
		totalModels += 1;
	}

	public String toString () {
		return "Context_" + identifier;
	}

	private List<Double> results;
	public List<Double> apply (FiveFourExperiment expt) {
		if (results == null) {
			results = new ArrayList<Double> ();

			for (Instance instance : expt.allInstances) {
				results.add (computeProbabilityA (instance, (FiveFourExperiment)expt));
			}
		}
		return results;
	}

	// -------- utility methods
	private double computeProbabilityA (Instance instance, FiveFourExperiment expt) {
		if (guessing > r.nextDouble ()) {
			 // return 1.0 (A) or 0.0 (B) randomly when guessing
			return (r.nextDouble () > 0.5 ? 1.0 : 0.0);
		} else {
			double eta_a = computeEta (instance, expt.training, "A");
			double eta_b = computeEta (instance, expt.training, "B");

			return eta_a / (eta_a + eta_b);
		}
	}

	private double computeEta (
			Instance givenInstance, 
			List<InstanceCategoryPair> trainingSet,
			String category
			) {
		double total_distance = 0.0;
		for (InstanceCategoryPair icp : trainingSet) {
			if (icp.category().equals (category)) {
				for (Instance instance : icp.instances ()) {
					total_distance += Math.exp (- computeDistance (
								givenInstance, instance));
				}
			}
		}
		return total_distance;
	}
}

