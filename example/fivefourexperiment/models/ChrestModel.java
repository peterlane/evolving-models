package fivefourexperiment.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.Icon;

import fivefourexperiment.FiveFourExperiment;
import fivefourexperiment.Instance;
import fivefourexperiment.InstanceCategoryPair;

import info.peterlane.mdk.optimisation.Model;

/** Chrest model provides a basic implementation, using lists for the two pattern types.  
  * The probability of responding with category A is obtained by training a 
  * population of models, and then extracting the number of individual models 
  * which classify an instance in category A.
  */
public class ChrestModel implements Model {
	private static int totalModels;
	private final int identifier;
	private static final Random r = new Random ();
	
	// basic parameters defining the model
	public double 
		rho;		// probability of learning a pattern
	private int 
		visualStmSize, // size of visual STM
		verbalStmSize, // size of verbal STM
		cycles,	// number of training cycles
		populationSize; // number of individuals in population
	
	public static final int
		MAX_STM = 10, // maximum allowed STM size
		MAX_CYCLES = 1000, // maximum allowed number of training cycles
		MAX_POPULATION = 100; // maximum number of individuals to train

	public ChrestModel () {
		identifier = totalModels;
		totalModels += 1;
		rho = r.nextDouble ();
		visualStmSize = r.nextInt (MAX_STM) + 1; // between 1 and MAX_STM inclusive
		verbalStmSize = r.nextInt (MAX_STM) + 1; // between 1 and MAX_STM inclusive
		cycles = r.nextInt (MAX_CYCLES) + 1; // between 1 and MAX_CYCLES inclusive
		populationSize = r.nextInt (MAX_POPULATION) + 1;
	}

	public double getRho () { return rho; }
	public int getVisualStm () { return visualStmSize; }
	public int getVerbalStm () { return verbalStmSize; }
	public int getCycles () { return cycles; }
	public int getPopulationSize () { return populationSize; }
	public String toString () { 
		return "Chrest_" + identifier;
	}
	
	public boolean equals (Object o) {
		if (!(o instanceof ChrestModel)) return false;
		ChrestModel model = (ChrestModel)o;

		return (rho == model.rho &&
			cycles == model.cycles &&
			populationSize == model.populationSize);
	}

	// keep a cache of results, to save recomputing
	private List<Double> results;
	public List<Double> apply (FiveFourExperiment expt) {
		if (results == null) {
			results = new ArrayList<Double> ();

			// create a pool of models
			List<Chrest> models = new ArrayList<> ();
			for (int i = 0; i < populationSize; ++i) {
				models.add (new Chrest (rho, visualStmSize, verbalStmSize));
			}
			// train them
			for (Chrest model : models) {
				model.train (expt, cycles);
			}
			// get proportion saying A for each test instance
			for (Instance instance : expt.allInstances) {
				int numCategoryA = 0;
				for (Chrest model : models) {
					if (model.inCategoryA (instance)) numCategoryA += 1;
				}

				results.add (((double)numCategoryA / (double)populationSize));
			}
		}

		return results;
	}

  public int getParameterCount () {
    return 5;
  }

	public String getParameterName (int parameter) {
    String[] names = { "Rho", "Visual STM", "Verbal STM", "Learning cycles", "Population size" };
    return names[parameter];
	}

	public double getParameter (int parameter) {
		switch (parameter) {
			case 0: return getRho ();
			case 1: return getVisualStm ();
			case 2: return getVerbalStm ();
			case 3: return getCycles ();
			default: // case 4:
				return getPopulationSize ();
		}
	}

  public void setParameter (int parameter, double value) {
		switch (parameter) {
			case 0: rho = value; break;
			case 1: visualStmSize = (int)value; break;
			case 2: verbalStmSize = (int)value; break;
			case 3: cycles = (int)value; break;
			default: // case 4:
				populationSize = (int)value; break;
		}
	}

		public void checkParameters () {
		if (rho < 0.0) rho = 0.0;
		if (rho > 1.0) rho = 1.0;
		if (visualStmSize < 1) visualStmSize = 1;
		if (visualStmSize > MAX_STM) visualStmSize = MAX_STM;
		if (verbalStmSize < 1) verbalStmSize = 1;
		if (verbalStmSize > MAX_STM) verbalStmSize = MAX_STM;
		if (cycles < 1) cycles = 1;
		if (cycles > MAX_CYCLES) cycles = MAX_CYCLES;
		if (populationSize < 1) populationSize = 1;
		if (populationSize > MAX_POPULATION) populationSize = MAX_POPULATION;
	}

  /**
   * Public static method provided to give icon for GUI.
   */
  public static Icon getIcon () {
    return new ImageIcon (ChrestModel.class.getResource("icons/ChrestModel.gif"));
  }
}

class Chrest {
	private static final Random r = new Random ();
	private double rho;
	private Node root;
	private Stm visualStm;
	private Stm verbalStm;

	public Chrest (double rho, int visualStmSize, int verbalStmSize) {
		this.rho = rho;
		root = new Node (new Pattern ("Root"));
		visualStm = new Stm (visualStmSize);
		verbalStm = new Stm (verbalStmSize);
	}

	public void train (FiveFourExperiment expt, int numCycles) {
		for (int i = 0; i < numCycles; ++i) { 
			train (expt);
		}
	}

	private void train (FiveFourExperiment expt) {
    for (InstanceCategoryPair icp : expt.training) {
      for (Instance instance : icp.instances ()) {
        trainInstance (makeVisualPattern (instance), makeVerbalPattern (icp.category ()));
      }
    }
	}

	private void trainInstance (Pattern instance, Pattern target) {
		if (r.nextDouble () > rho) return; // don't learn in (1 - rho) cases
		
		visualStm.addNode (root.find(instance).learn (instance));
		verbalStm.addNode (root.find(target).learn (target));
		if (visualStm.isEmpty ()) return; // check not empty
		if (verbalStm.isEmpty ()) return; // check not empty
		if (visualStm.getTopNode () == verbalStm.getTopNode ()) return; // both could hold root
		// add a two-way naming link
		visualStm.getTopNode().setNamingLink (verbalStm.getTopNode ());
		verbalStm.getTopNode().setNamingLink (visualStm.getTopNode ());
	}

	/** Given instance is in category A if retrieved node linked to verbal pattern "A". */
	public boolean inCategoryA (Instance instance) {
		Node foundNode = root.find (makeVisualPattern (instance));
		return ( (foundNode.getNamingLink () != null) &&
			 (foundNode.getNamingLink().getImage().equals (makeVerbalPattern ("A"))) );
	}

	private Pattern makeVerbalPattern (String name) {
		List<String> items = new ArrayList<String> ();
		items.add ("Verbal");
		items.add (name);
		return new Pattern (items);
	}

	private Pattern makeVisualPattern (Instance instance) {
		List<String> items = new ArrayList<String> ();
		items.add ("Visual");
    items.add ("" + instance.getA0AsInt ());
    items.add ("" + instance.getA1AsInt ());
    items.add ("" + instance.getA2AsInt ());
    items.add ("" + instance.getA3AsInt ());
		
		return new Pattern (items);
	}

	class Link {
		private Pattern test;
		private Node child;

		public Link (Pattern test, Node child) {
			this.test = test;
			this.child = child;
		}

		public Pattern getTest () {
			return test;
		}

		public Node getChild () {
			return child;
		}
	}

	class Node {
		private Pattern contents;
		private Pattern image;
		private Node namingLink;
		private List<Link> children;

		public Node (Pattern contents) {
			this.contents = contents;
			image = new Pattern ();
			namingLink = null;
			children = new ArrayList<Link> ();
		}

		public Pattern getImage () {
			return image;
		}

		public Node getNamingLink () {
			return namingLink;
		}

		public void setNamingLink (Node node) {
			namingLink = node;
		}

		// learn the given pattern, assuming that this node is where the pattern was 
		// sorted to
		public Node learn (Pattern pattern) {
			if (image.matches (pattern)) {
				return familiarise (pattern);
			} // else 
			return discriminate (pattern);
		}

		// assume the image matches the given pattern
		// take the next item and add it to the image
		private Node familiarise (Pattern pattern) {
			image = image.addFeature (pattern.remove (image));

			return this;
		}

		private Node discriminate (Pattern pattern) {
			Pattern test = new Pattern (pattern.getItem (0)); // create empty pattern with same modality
			test = test.addFeature (pattern.remove (contents)); // add a single feature from pattern
			if (test.getSize () == 2) { // add child if test is modality plus item
				Node child = new Node (contents.addFeature (test));

				children.add (new Link (test, child));
				return child;
			}

			return this;
		}

		public Node find (Pattern pattern) {
			// subPattern is part of pattern with contents removed from front
			// -- we assume that contents matches the pattern
			Pattern subPattern = pattern.remove (contents);
			// work through the children, and descend into a matching link test
			for (Link link : children) {
				if (link.getTest().matches (subPattern)) {
					return link.getChild().find (pattern);
				}
			}
			// did not find a matching child, so return this node
			return this;
		}
	}

	class Stm {
		private List<Node> nodes;
		private final int size;

		public Stm (int size) {
			this.size = size;
			nodes = new ArrayList<Node> ();
		}

		/** If node not in STM, add it to top -- position 0.
		    If node is in STM, then remove it, and still add to top.
		    Finally, remove any nodes beyond index 'size'. 
		 */
		public void addNode (Node node) {
			// remove node if already present (in effect, bring node to top of STM)
			if (nodes.contains (node)) {
				nodes.remove (node);
			}
			// add node to top position
			nodes.add (0, node);
			// make sure nodes is no longer than 'size'
			while (nodes.size() > size) {
				nodes.remove (nodes.size () - 1);
			}
		}

		public boolean isEmpty () {
			return nodes.isEmpty ();
		}
		
		/** Returns top node -- assumes that nodes is not empty. */
		public Node getTopNode () {
			return nodes.get (0);
		}
	}
	
	// inner class defines the pattern class - a list pattern
	// uses first item to distinguish 'visual' 'verbal'
	class Pattern {
		List<String> items;
		
		public Pattern () {
			items = new ArrayList<String> ();
		}

		public Pattern (String item) {
			items = new ArrayList<String> (); 
			items.add (item);
		}
		
		public Pattern (List<String> items) {
			this.items = items;
		}

		public boolean isVisual () {
			if (items.isEmpty ()) return false;
			return items.get(0).equals ("Visual");
		}

		public boolean isVerbal () {
			if (items.isEmpty ()) return false;
			return items.get(0).equals ("Verbal");
		}

		int getSize () { return items.size (); }
		String getItem (int n) { return items.get (n); }

		// two patterns are equal if every item is the same, and patterns same size
		public boolean equals (Pattern pattern) {
			if (this.getSize () != pattern.getSize () ) return false;
			for (int i = 0, n = this.getSize (); i < n; ++i) {
				if (!this.getItem(i).equals (pattern.getItem (i))) return false;
			}
		
			return true;
		}

		// this pattern matches a given pattern if every item in this is the same
		public boolean matches (Pattern pattern) {
			if (this.getSize () > pattern.getSize ()) return false;
			for (int i = 0, n = this.getSize (); i < n; ++i) {
				if (!this.getItem(i).equals (pattern.getItem (i))) return false;
			}

			return true;
		}

		public Pattern remove (Pattern pattern) {
			// assume given pattern matches this pattern
			// -- create new pattern from current items beyond given's size
			List<String> items = new ArrayList<String> ();
			items.add (this.getItem (0)); // add the 'modality' information'
			if (this.getSize () > pattern.getSize ()) {
				for (int i = pattern.getSize (), n = this.getSize (); i < n; ++i) {
					items.add (this.getItem (i));
				}
			}


			return new Pattern (items);
		}

		// Add next feature from given pattern to this pattern, returning a new pattern
		public Pattern addFeature (Pattern pattern) {
			List<String> items = new ArrayList<String> ();
			items.addAll (items); // keep this pattern's items
			if (pattern.getSize () > 1) { // i.e. is modality plus more information
				// add first feature from given pattern after modality
				items.add (pattern.getItem (1)); 
			}
			return new Pattern (items);
		}

		public String toString () {
			String result = "Pattern: ";
			for (String item : items) {
				result += item + " ";
			}
			return result;
		}
	}

}

