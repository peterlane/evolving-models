package fivefourexperiment.models;

import java.util.ArrayList;
import java.util.List;

import fivefourexperiment.FiveFourExperiment;
import fivefourexperiment.Instance;
import info.peterlane.mdk.optimisation.Model;

public class PrototypeModel extends MathematicalModel {
	private static int totalModels;
	private static Instance 
		aPrototype = new Instance ("A", true, true, true, true),
		bPrototype = new Instance ("B", false, false, false, false);
	
	public PrototypeModel () {
		super (totalModels);
		totalModels += 1;
	}
	
	public String toString () {
		return "Prototype_" + identifier;
	}

	private List<Double> results;
	public List<Double> apply (FiveFourExperiment expt) {
		if (results == null) {
			results = new ArrayList<Double> ();

			for (Instance instance : (expt).allInstances) {
				results.add (computeProbabilityA (instance));
			}
		}
		
		return results;
	}
	
	// -------- utility methods
	private double computeProbabilityA (Instance instance) {
		if (guessing > r.nextDouble ()) {
			 // return 1.0 (A) or 0.0 (B) randomly when guessing
			return (r.nextDouble () > 0.5 ? 1.0 : 0.0);
		} else {
			double eta_a = computeEta (instance, aPrototype);
			double eta_b = computeEta (instance, bPrototype);

			return eta_a / (eta_a + eta_b);
		}
	}

	private double computeEta (Instance i1, Instance i2) {
		return Math.exp (- computeDistance (i1, i2));
	}
}

