package fivefourexperiment;

import java.util.ArrayList;
import java.util.List;

import info.peterlane.mdk.optimisation.Constraint;

abstract class SimpleConstraint implements Constraint {
  String name;
  double[] targets;

  public SimpleConstraint (String name, double[] targets) {
    this.name = name;
    this.targets = targets;
  }
  
  public List<Double> getTargets () {
    List<Double> result = new ArrayList<> ();
    for (double value : targets) {
      result.add (value);
    }
    return result;
  }
}
