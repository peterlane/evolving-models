package fivefourexperiment;

import java.util.ArrayList;
import java.util.List;

public class SseConstraint extends SimpleConstraint {
	
	public SseConstraint (String name, double[] targets) {
    super (name, targets);
	}
	
  public double computeMatch (List<Double> results) {
		double sse = 0.0;
		for (int i = 0, n = Math.min(results.size (), targets.length); i < n; i+=1) {
			if (results.get (i) == null) {
				; // ignore this error
			} else {
				sse += (results.get (i) - targets[i]) * 
					(results.get (i) - targets[i]);
			}
		}
		return sse;
	}

	public String toString () {
		return "SSE " + name;
	}
}

