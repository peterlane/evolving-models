package fivefourexperiment;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.Tools;
import fivefourexperiment.FiveFourExperiment;

/**
 * An example of a view added by the specific experiment to 
 * the GUI environment.  This view displays all the instances 
 * and target constraint values.
 *
 * @author Peter Lane
 */
public class DataView extends JInternalFrame {
	private final FiveFourExperiment expt;

	public DataView (FiveFourExperiment expt ) {
		super ("Data View", true, true, true, true);
		
		this.expt = expt;

		JLabel label = new JLabel ("Experimental data and constraints:");
		label.setBorder (new EmptyBorder (3, 3, 3, 3));
		add (label, BorderLayout.NORTH);
		if (expt.getConstraints().size () == 0) {
			add (new JLabel ("NO CONSTRAINTS !"), BorderLayout.CENTER);
		} else {
			add (new JScrollPane (createTable ()), BorderLayout.CENTER);
		}
		
		setMinimumSize (new Dimension (300, 200));
		setSize (300, 200);
		setVisible (true);
	}

	// caller ensures that there are some constraints
	private JTable createTable () {
		JTable table = new JTable (new ConstraintTableModel (getInstanceNames (), 
					createData (), createHeaders ()));
		Tools.addStandardRenderers (table);
		table.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
		// make headers use a bold font
		table.getTableHeader().setFont (table.getTableHeader().getFont().deriveFont (Font.BOLD));

		return table;
	}

  /**
   * This view is only used for 5-4 task, so we can assume the experiment instance
   * returns constraints derived from SimpleConstraint.
   */
	private double[][] createData () {
    int numTargets = ((SimpleConstraint)(expt.getConstraints().get(0))).getTargets().size ();
		double[][] data = new double[numTargets][expt.getConstraints().size ()];
		for (int i = 0, n = expt.getConstraints().size (); i < n; ++i) {
			for (int j = 0; j < numTargets; ++j) {
				data[j][i] = ((SimpleConstraint)(expt.getConstraints().get(i))).getTargets().get (j);
			}
		}

		return data;
	}

	// first header is for data instance, rest are based on constraint name
	private String[] createHeaders () {
		String[] headers = new String[expt.getConstraints().size ()+1];

		headers[0] = "Instance";
		for (int i = 0, n = expt.getConstraints().size (); i < n; ++i) {
			headers[i+1] = expt.getConstraints().get(i).toString ();
		}

		return headers;
	}

	private String[] getInstanceNames () {
		List<String> names = new ArrayList<> ();

		for (Instance instance : expt.allInstances) {
			names.add (instance.toString ());
		}
		return names.toArray (new String[0]);
	}
}

class ConstraintTableModel extends DefaultTableModel {
	private final double[][] data;
	private final String[] headers;
	private final String[] rowNames;
	
	public ConstraintTableModel (String[] instanceNames, double[][] data, String[] headers) {
		super (instanceNames.length, headers.length);
		this.data = data;
		this.headers = headers;
		rowNames = instanceNames;
	}

	public Class getColumnClass (int c) {
		return (c == 0 ? String.class : double.class);
	}

	public String getColumnName (int c) {
		return headers[c];
	}

	public Object getValueAt (int r, int c) {
		if (c == 0) {
			return rowNames[r];
		} else {
			return data[r][c-1];
		}
	}

	public boolean isCellEditable (int row, int col) {
		return false;
	}
}

