package fivefourexperiment;

import info.peterlane.mdk.optimisation.gui.Shell;

/**
 * Start an instance of the graphical environment tailored for the 5-4 task.
 *
 * @author Peter Lane
 */
public class FiveFourExperimentDemo {
  /** Get the interface started in its own GUI thread */
  public static void main (String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      FiveFourExperiment expt = new FiveFourExperiment ();
      public void run() { new Shell (expt, new DataView (expt)); }
    });
  }

}
