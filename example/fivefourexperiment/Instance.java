package fivefourexperiment;

import java.util.ArrayList;
import java.util.List;

/** A data structure to hold instances of the Five-Four experiment. */
public class Instance {
	private final String _name;
	private final boolean _a0, _a1, _a2, _a3;

	public Instance (String name, 
			boolean a0, boolean a1, boolean a2, boolean a3) {
		_name = name;
		_a0 = a0;
		_a1 = a1;
		_a2 = a2;
		_a3 = a3;
	}

	public String toString () {
		return _name + ": " + 
			getA0AsInt () + " " +
			getA1AsInt () + " " +
			getA2AsInt () + " " +
			getA3AsInt ();
	}

	public boolean sameA0 (Instance i) {
		return _a0 == i._a0;
	}

	public boolean sameA1 (Instance i) {
		return _a1 == i._a1;
	}

	public boolean sameA2 (Instance i) {
		return _a2 == i._a2;
	}

	public boolean sameA3 (Instance i) {
		return _a3 == i._a3;
	}

	public int getA0AsInt () { return (_a0 ? 1 : 0); }
	public int getA1AsInt () { return (_a1 ? 1 : 0); }
	public int getA2AsInt () { return (_a2 ? 1 : 0); }
	public int getA3AsInt () { return (_a3 ? 1 : 0); }
}

