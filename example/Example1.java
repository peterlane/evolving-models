
import java.util.ArrayList;
import java.util.List;

import fivefourexperiment.models.ContextModel;
import fivefourexperiment.AadConstraint;
import fivefourexperiment.FiveFourExperiment;
import fivefourexperiment.Instance;
import fivefourexperiment.InstanceCategoryPair;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.algorithms.SingleTheorySingleConstraint;

/**
 * Simple example of using the evolving-models library to optimise 
 * the parameters for a Context Model against a single experimental 
 * constraint.
 * Uses the models from 'fivefourexperiment'.
 *
 * @author Peter Lane
 */
public class Example1 {
  public static void main (String[] args) {
    // create an instance of an Experiment
    MyExperiment expt = new MyExperiment ();
    // create an instance of the evolutionary algorithm
    SingleTheorySingleConstraint stsc = new SingleTheorySingleConstraint (expt);

    try {
      // initialise the algorithm with a population of 100 individuals
      stsc.create (100);
      // evolve the population for 100 cycles, 
      // keeping top 10% as elite models
      // and mutating 10%
      for (int i = 0; i < 100; i++) {
        stsc.evolve (10, 10);
      }
    } catch (IllegalArgumentException iae) {
      System.out.println ("Caught an error in the arguments: " + iae);
    }

    // display the final models and performance
    List<Model> bestModels = stsc.getEliteModels (10); // top 10%
    for (Model model : bestModels) {
      System.out.println ("Model: " + model);
      for (int i = 0; i < model.getParameterCount (); i++) {
        System.out.println ("  " + model.getParameterName (i) + " = " + model.getParameter (i));
      }
      System.out.println ("");
      System.out.println ("  Performance: " + expt.getConstraints().get(0).computeMatch (expt.evaluate (model)));
    }
  }
}

/**
 * The Experiment class must implement the 'Experiment' interface.
 * (In this example, we extend FiveFourExperiment because the models 
 * are designed to read the training/allInstances fields.)
 */
class MyExperiment extends FiveFourExperiment {

  // fields to hold training and test data for model
  public final List<InstanceCategoryPair> training; // make visible to models
	public final List<Instance> allInstances;  // make visible to models

	public MyExperiment () {
    // setup the training and test data
		training = new ArrayList<InstanceCategoryPair> ();
		allInstances = new ArrayList<Instance> ();

    List<Instance> categoryA = new ArrayList<> ();
    categoryA.add (new Instance ("E1", true, true, true, false));
		categoryA.add (new Instance ("E2", true, false, true, false));
		categoryA.add (new Instance ("E3", true, false, true, true));
		categoryA.add (new Instance ("E4", true, true, false, true));
		categoryA.add (new Instance ("E5", false, true, true, true));
    addTraining (categoryA, "A");

    List<Instance> categoryB = new ArrayList<> ();
    categoryB.add (new Instance ("E6", true, true, false, false));
		categoryB.add (new Instance ("E7", false, true, true, false));
		categoryB.add (new Instance ("E8", false, false, false, true));
		categoryB.add (new Instance ("E9", false, false, false, false));
    addTraining (categoryB, "B");

		addTestItem (new Instance ("E10", true, false, false, true));
		addTestItem (new Instance ("E11", true, false, false, false));
		addTestItem (new Instance ("E12", true, true, true, true));
		addTestItem (new Instance ("E13", false, false, true, false));
		addTestItem (new Instance ("E14", false, true, false, true));
		addTestItem (new Instance ("E15", false, false, true, true));
		addTestItem (new Instance ("E16", false, true, false, false));
	}

  /**
   * Required by Experiment interface: returns a list of the constraints 
   * available for this simulation experiment.
   */
  public List<Constraint> getConstraints () {
    List<Constraint> constraints = new ArrayList<> ();

    constraints.add (new AadConstraint (
          "AAD 1", 
          new double[]{ 0.78, 0.88, 0.81, 0.88, 0.81, 0.16, 0.16, 0.12,
            0.03, 0.59, 0.31, 0.94, 0.34, 0.50, 0.62, 0.16 }));

    return constraints;
  }
  
  /**
   * Required by Experiment interface: returns a list of the theories 
   * (model classes) to use in the current simulation experiment.  
   * In this case, we only have one theory, the class of Context Models.
   */
  public List<Class<? extends Model>> getTheories () {
    List<Class<? extends Model>> theories = new ArrayList<> ();

    theories.add (ContextModel.class);

    return theories;
  }
  
  /**
   * Required by Experiment interface: requests model to build/train itself
   * given its current set of parameters, and return a list of 
   * results against the test constraints.
   */
  public List<Double> evaluate (Model model) {
    // we know we must have a context model
    // evaluate model by calling its 'apply' method with this experiment
    return ((ContextModel)model).apply (this);
  }

	// private methods to assist creation of datasets by automatically
	// adding item to respective datasets

	private void addTraining (List<Instance> instances, String category) {
		training.add (new InstanceCategoryPair (instances, category));
    for (Instance instance : instances) {
  		allInstances.add (instance);
    }
	}

	private void addTestItem (Instance instance) {
		allInstances.add (instance);
	}
}

