/**
 * A suite of optimisation algorithms and utility classes/interfaces 
 * for evolving sets of models drawn from multiple theories tested 
 * against multiple constraints.  
 *
 * Experiments are based around Models, which have a number of 
 * parameters to be optimised, and Constraints, which are the levels 
 * of fitness to optimise against.  An Experiment manages the models 
 * and constraints.
 *
 * @author Peter Lane
 */
package info.peterlane.mdk.optimisation;
