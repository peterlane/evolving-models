package info.peterlane.mdk.optimisation;

/**
* Provides a static method for extracting class name from a class object.
*
* @author Peter Lane
*/
public class Theory {
	public static String classNameFor (Class<? extends Model> theory) {
		int p = theory.getName().lastIndexOf ('.') + 1;
		return theory.getName().substring(p);
	}
}
