package info.peterlane.mdk.optimisation;

import java.util.List;

/**
 * A constraint is used to measure the performance of a model 
 * against that of humans.  Each constraint must include a 
 * set of human data and a formula to compute the fit of 
 * the model's responses to the human data.  An example 
 * would be to compute the sum-squared error of the model's 
 * responses when compared with the humans.
 *
 * @author Peter Lane
 */
public interface Constraint {
  /**
   * Compute a level of fit between the given model responses 
   * and a set of human data. Levels of fit must be positive 
   * numbers, with a value of 0 representing a perfect fit, and 
   * larger values increasingly poor levels of fit.
   *
   * @param results a list of responses from the model on the 
   * set of data.  {@link Experiment#evaluate(Model)} provides 
   * the responses which are then passed as input.
   */
  double computeMatch (List<Double> results);
}

