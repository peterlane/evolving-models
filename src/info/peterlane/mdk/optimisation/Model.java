package info.peterlane.mdk.optimisation;

import java.util.List;

/**
 * Requires implementing classes to provide access to parameters 
 * of model.  Currently, only double values are supported, 
 * although these may be interpreted/converted as appropriate.
 *
 * @author Peter Lane
 */
public interface Model {

	/** Retrieve parameter name given an index to the parameter. */
	String getParameterName (int parameter);

	/** Retrieve count of parameters. */
  int getParameterCount ();

	/** Retrieve parameter value given an index to the parameter. */
	double getParameter (int parameter);

	/** Set parameter value given its index. */
  void setParameter (int parameter, double value);

	/** Check parameters are within bounds or properly normalised. */
	void checkParameters ();
}

