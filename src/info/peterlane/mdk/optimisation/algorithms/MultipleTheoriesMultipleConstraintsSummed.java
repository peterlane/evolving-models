package info.peterlane.mdk.optimisation.algorithms;

import java.util.*;

import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/**
 * Optimisation algorithm for multiple theories and multiple criteria, using the 
 * summed fitness of all the constraints to rank the models.  This algorithm 
 * uses one instance of 
 * {@link info.peterlane.mdk.optimisation.algorithms.SingleTheoryMultipleConstraintsSummed} per 
 * theory.
 *
 * @author Peter Lane
 */
public class MultipleTheoriesMultipleConstraintsSummed extends GeneticAlgorithm {
	private final Map<Class<? extends Model>, SingleTheoryMultipleConstraintsSummed> populations;
  private final Experiment expt;

	public MultipleTheoriesMultipleConstraintsSummed (Experiment expt) throws IllegalArgumentException {
    this.expt = expt;
		
		populations = new HashMap<Class<? extends Model>, SingleTheoryMultipleConstraintsSummed> ();
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.put (theory, new SingleTheoryMultipleConstraintsSummed (expt, theory));
		}
	}

	public void create (int size) {
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.get(theory).create (size);
		}
		cycles = 0;
	}

	/**
	 * @param elite The proportion of elite individuals to retain in each cycle,
	 * which must be a positive integer less than 100.
	 * @throws IllegalArgumentException if elite or mutate are out of range.
	 */
  public void evolve (int elite, int mutate) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.get(theory).evolve (elite, mutate);
		}
		cycles += 1;
	}

	public void notifyObservers () {
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.get(theory).notifyObservers ();
		}
	}
	
	public SingleTheoryMultipleConstraintsSummed getPopulation (Class<? extends Model> theory) {
		return populations.get (theory);
	}

	public String toString () {
		return "MTMC [summed] (after " + cycles + " cycle" + 
			((cycles == 1) ? "" : "s") +
			")";
	}
}

