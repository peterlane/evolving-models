package info.peterlane.mdk.optimisation.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/**
 * Optimisation algorithm for multiple theories against a single experimental 
 * constraint.  The algorithm essentially maintains multiple instances of  
 * {@link info.peterlane.mdk.optimisation.algorithms.SingleTheorySingleConstraint}, one per theory.
 *
 * @author Peter Lane
 */
public class MultipleTheoriesSingleConstraint extends GeneticAlgorithm {
	private final Map<Class<? extends Model>, SingleTheorySingleConstraint> populations;
  private final Experiment expt;

	public MultipleTheoriesSingleConstraint (Experiment expt) throws IllegalArgumentException {
    if (expt.getConstraints().isEmpty ()) {
      throw new IllegalArgumentException ("Experiment has no constraints");
    }

    this.expt = expt;
		
		populations = new HashMap<Class<? extends Model>, SingleTheorySingleConstraint> ();
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.put (theory, new SingleTheorySingleConstraint (expt, expt.getConstraints().get (0), theory));
		}
	}

	public void create (int size) {
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.get(theory).create (size);
		}
		cycles = 0;
	}

	/**
	 * @param elite The number of elite individuals to retain in each cycle, 
	 * which must be a positive integer less than the population size.
	 * @throws IllegalArgumentException if elite or mutate are out of range.
	 */
	public void evolve (int elite, int mutate) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.get(theory).evolve (elite, mutate);
		}
		cycles += 1;
	}

	public void notifyObservers () {
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.get(theory).notifyObservers ();
		}
	}
	
	public SingleTheorySingleConstraint getPopulation (Class<? extends Model> theory) {
		return populations.get (theory);
	}

	public String toString () {
		return "MTSC (after " + cycles + " cycle" + 
			((cycles == 1) ? "" : "s") +
			")";
	}
}

