package info.peterlane.mdk.optimisation.algorithms;

import java.util.*;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;
import info.peterlane.mdk.optimisation.algorithms.comparators.ModelFitnessComparator;

/**
 * Provides an implementation of an optimisation algorithm for developing models 
 * developed within a single theory against a single constraint.
 *
 * @author Peter Lane
 */
public class SingleTheorySingleConstraint extends SingleCriterion {

	private Constraint constraint;

  /**
   * Constructor takes constraint and theory from given experiment, 
   * for an optimisation algorithm to handle a single theory and 
   * single constraint.
   *
   * @throws IllegalArgumentException if expt does not contain a constraint or theory
   */
	public SingleTheorySingleConstraint (Experiment expt) throws IllegalArgumentException {
		super (expt);

    if (expt.getConstraints().isEmpty ()) {
      throw new IllegalArgumentException ("No constraints");
    }
    
		constraint = expt.getConstraints().get (0);
  }

	public SingleTheorySingleConstraint (Experiment expt, Class<? extends Model> theory) throws IllegalArgumentException {
		super (expt, theory);
	}

  /**
   * Constructor takes explicit constraint and theory, 
   * for an optimisation algorithm to handle a single theory and 
   * single constraint.
   */
  public SingleTheorySingleConstraint (
      Experiment expt, 
      Constraint constraint, 
      Class<? extends Model> theory) {

		super (expt, theory);

		this.constraint = constraint;
  }

	public String toString () {
		if (theory == null || constraint == null) return "NO POPULATION";

		return Theory.classNameFor (theory) + " " + constraint.toString() + " " + getCyclesAsString ();
	}

  public void setConstraint (Constraint constraint) {
    constraint = constraint;
  }

	public List<Double> getFitnessValuesHistory () { 
		List<Double> results = new ArrayList<> ();

		if (constraint != null && expt != null) {
			for (Model model : bestModels) {
				results.add (constraint.computeMatch (expt.evaluate (model)));
			}
		}
		
		return results; 
	}

	protected void sortPopulation () {
		java.util.Collections.sort (population,
				new ModelFitnessComparator (expt, constraint));
	}
}

