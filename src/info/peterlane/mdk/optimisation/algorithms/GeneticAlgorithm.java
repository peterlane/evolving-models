package info.peterlane.mdk.optimisation.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import info.peterlane.mdk.optimisation.Model;

/**
 * Provides methods supported by all optimisation algorithms 
 * to create, evolve and query basic properties of a 
 * population of Model instances.
 *
 * @author Peter Lane
 */
abstract public class GeneticAlgorithm extends java.util.Observable {
	private static final Random random = new Random ();

  public int getCycles () {
    return cycles;
  }

  protected int cycles;

  protected String getCyclesAsString () {
    return "(after " + cycles + " cycle" +
			(cycles == 1 ? "" : "s") +
			")";
  }

	/**
	 * Instructs algorithm to create a population of individuals
	 * of given size.
	 */
	abstract public void create (int size);

	/**
	 * Instructs algorithm to evolve current population.
	 * @param mutate A number from 0 to 100 giving the proportion
	 * of individuals to mutate.
	 */
	abstract public void evolve (int x, int mutate)
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException;

	protected void createPopulation (List<Model> population,
			List<Model> pool, int mutateProbability, int populationSize) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
		
		// make up surplus by cross-over
		while (population.size () < populationSize) {
			int parent1 = random.nextInt (pool.size ());
			int parent2 = random.nextInt (pool.size ());
			for (int safetyNet = 0; safetyNet < 10 && parent1 == parent2; ++safetyNet) {
				parent2 = random.nextInt (pool.size ());
			}

			Parents parents = new Parents (
					pool.get (parent1), 
					pool.get (parent2),
					mutateProbability);
			if (!population.contains (parents.child1)) {
				population.add (parents.child1);
			}
			if (population.size () < populationSize) {
				if (!population.contains (parents.child2)) {
					population.add (parents.child2);
				}
			}
		}
	}

	class Parents {
		private Model child1;
		private Model child2;

		protected Parents (Model parent1, Model parent2, int mutateProbability)
        throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
			List<Number> genome1 = getGenome (parent1);
			List<Number> genome2 = getGenome (parent2);
			int crosspoint = random.nextInt (genome1.size ());

			child1 = createInstance (parent1, mutate (
						crossOver (crosspoint, genome1, genome2),
						mutateProbability));
			child2 = createInstance (parent2, mutate (
						crossOver (crosspoint, genome2, genome1),
						mutateProbability));
		}

    private List<Number> getGenome (Model model) throws IllegalArgumentException {
      List<Number> values = new ArrayList<> ();

      for (int i=0; i < model.getParameterCount (); i += 1) {
        values.add (Double.valueOf (model.getParameter (i)));
      }

      return values;
    }

    private Model createInstance (Model model, List<Number> values) 
        throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
      Model newmodel;

      try {
        Class<?> c = model.getClass ();
        newmodel = (Model)(c.getDeclaredConstructor().newInstance ());

        for (int i=0; i < values.size (); i += 1) {
          newmodel.setParameter (i, (double)(values.get (i)));
        }
				newmodel.checkParameters ();
      } catch (IllegalAccessException | InstantiationException ie) {
        newmodel = null;
      }
      return newmodel;
    }

		private List<Number> crossOver (int posn, List<Number> genome1, List<Number> genome2) {
			List<Number> result = new ArrayList<Number> ();
			for (int i = 0; i < posn; ++i) {
				result.add (genome1.get (i));
			}
			for (int i = posn; i < genome2.size (); ++i) {
				result.add (genome2.get (i));
			}
			return result;
		}

		private List<Number> mutate (List<Number> genome, int mutateProbability) {
			if (random.nextInt (100) < mutateProbability) { // do mutation with given probability
				int gene = random.nextInt (genome.size ());
				if (genome.get (gene) instanceof Integer) {
					genome.set (gene, (Integer)(genome.get (gene)) + random.nextInt (2) - 1);
				} else {
					genome.set (gene, (Double)(genome.get (gene)) + random.nextDouble () - 0.5);
				}
			}

			return genome;
		}
	}
}
