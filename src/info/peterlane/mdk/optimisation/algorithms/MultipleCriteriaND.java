package info.peterlane.mdk.optimisation.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/**
 * Supports optimisation algorithms using non-dominated sorting 
 * across multiple experimental constraints.  
 *
 * @author Peter Lane
 */
public abstract class MultipleCriteriaND extends GeneticAlgorithm {

	protected static final Random random = new Random ();
	protected final Experiment expt;
  protected List<Model> nonDominatedModels = null;

  public MultipleCriteriaND (Experiment expt) {
    this.expt = expt;
  }

  protected void clearCollectedData () {
    cycles = 0;
    nonDominatedModels = null;
  }

  protected void updateCollectedData () {
    cycles += 1;
    nonDominatedModels = null;
  }

	public boolean isNonDominatedModel (Model model) {
		return getNonDominatedModels().contains (model);
	}

	/** 
   * non-dominated models placed into nonDominatedModels array, 
   * remaining models returned to caller
   */
	protected List<Model> separateNonDominatedModels (List<Model> models, 
			List<Model> nonDominatedModels) {
		List<Model> dominatedModels = new ArrayList<Model> ();

		for (Model model : models) {
			if (nonDominatedModel (model, models)) {
				nonDominatedModels.add (model);
			} else {
				dominatedModels.add (model);
			}
		}
		return dominatedModels;
	}

  	// model is not dominated in collection of models 
	// if it is not dominated by any individual model in that collection
	protected boolean nonDominatedModel (Model model, List<Model> models) {
		for (Model testModel : models) {
			if (testModel != model && // only check different models to given one
			    dominates (testModel, model)) {
				return false;
			}
		}
		return true;
	}

	/** Note, sometimes a problem with threading, as nonDominatedModels is 'nulled'
	  * between the call to 'get' and the test for 'contains', so check for 'null'
	  * TODO: Explore question of thread safety.
	  */
	public List<Model> getNonDominatedModels () {
		if (nonDominatedModels == null) {
			List<Model> newNDModels = new ArrayList<> ();
			separateNonDominatedModels (getModels (), newNDModels);
			nonDominatedModels = newNDModels;
		}
		return nonDominatedModels;
	}
  
  // model1 dominates model2 if it is at least as good as model2 everywhere,
	// and better in one constraint
	protected boolean dominates (Model model1, Model model2) {
		return (asGoodEveryWhere (model1, model2) &&
			betterOnce (model1, model2));
	}

	// assume matches are better when nearer to 0.0
	// model1 is not as good everywhere if it is worse in one constraint
	private boolean asGoodEveryWhere (Model model1, Model model2) {
		List<Double> results1 = expt.evaluate (model1);
		List<Double> results2 = expt.evaluate (model2);

		for (Constraint c : expt.getConstraints ()) {
			if (c.computeMatch (results1) > c.computeMatch (results2)) {
				return false;
			}
		}
		return true;
	}
  	// model1 is better once if it is strictly better in one constraint
	private boolean betterOnce (Model model1, Model model2) {
		List<Double> results1 = expt.evaluate (model1);
		List<Double> results2 = expt.evaluate (model2);

		for (Constraint c : expt.getConstraints ()) {
			if (c.computeMatch (results1) < c.computeMatch (results2)) {
				return true;
			}
		}
		return false;
	}

  abstract protected List<Model> getModels ();

	/**
	 * @param numberSets The number of sets to use for the non-dominated 
   * sort.  numberSets must be a positive integer. 
	 * @throws IllegalArgumentException if numberSets or mutate are out of range.
	 */
	protected void evolve (int numberSets, int mutate, Creator creator) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
    if (numberSets < 0) {
      throw new IllegalArgumentException ("Number of non-dominated sets must be greater than 0");
    }

		if (mutate < 0 || mutate > 100) {
			throw new IllegalArgumentException ("Invalid mutate parameter to evolve");
		}

		List<List<Model>> sets = new ArrayList<> ();
		for (int i = 0; i < numberSets; ++i) {
			sets.add (new ArrayList<Model> ());
		}

		List<Model> remainingModels = getModels ();
		for (int i = 0; i < numberSets - 1; ++i) {
			remainingModels = separateNonDominatedModels (remainingModels,
					sets.get (i));
		}
		sets.set (numberSets-1, remainingModels);
		
		// preserve non-dominated set
		nonDominatedModels = sets.get (0);
		// create new datasets
    creator.create (sets, mutate);

		updateCollectedData ();
		setChanged ();
	}

  abstract class Creator {
    abstract public void create (List<List<Model>> sets, int mutate) 
        throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException;
  }

  protected void createPopulationRandomModels (int size, List<Model> population, Class<? extends Model> theory) {
    population.clear ();
    clearCollectedData ();
    
    for (int i = 0; i < size; ++i) {
      try {
        population.add (theory.getDeclaredConstructor().newInstance ());
      } catch (IllegalAccessException | InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException ie) {
        ; // ignore
      }
    }
    setChanged ();
  }


  protected void createNewPopulation (List<Model> population, Class<? extends Model> theory, List<List<Model>> sets, int mutate) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
    int populationSize = population.size ();
    population.clear ();

    // set up a pool of models to select from, with frequency of occurrence
    // depending on the set number
    List<Model> pool = new ArrayList<> ();
    for (int i = 0; i < sets.size (); i += 1) {
      for (Model model : sets.get (i)) { // add examples of this theory to pool
        if (theory == model.getClass ()) {
          for (int j = 0, n = (int)Math.pow (2, sets.size () - i -1); j < n; j += 1) {
            pool.add (model);
          }
        }
      }
    }

    createPopulation (
        population,
        pool, 
        mutate, 
        populationSize); 
  }

}
