/** 
 * Implementation of genetic algorithms used for evolving theories.
 * Different algorithms are provided to optimise on theories with 
 * a single or multiple theories, single or multiple constraints, 
 * and, with multiple constraints, to either order by sum of their 
 * values or use non-dominated sorting.
 */

package info.peterlane.mdk.optimisation.algorithms;
