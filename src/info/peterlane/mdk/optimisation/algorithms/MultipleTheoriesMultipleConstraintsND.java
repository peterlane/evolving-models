package info.peterlane.mdk.optimisation.algorithms;

import java.util.*;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.algorithms.comparators.ModelFitnessComparator;

/**
 * Optimisation algorithm for multiple theories and multiple criteria, using 
 * non-dominated sorting to rank the models.  A separate population is 
 * managed for each theory.  The non-dominated sorting ranks the models using 
 * all models from all theories.  Methods allow the non-dominated models 
 * ranked per theory or overall to be retrieved for analysis.
 * This algorithm is also known as the "Speciated Non-Dominated Sorting 
 * Genetic Algorithm" (Lane and Gobet, 2005).
 *
 * @author Peter Lane
 */
public class MultipleTheoriesMultipleConstraintsND extends MultipleCriteriaND {

	private final Map<Class<? extends Model>, List<Model>> populations;

	public MultipleTheoriesMultipleConstraintsND (Experiment expt) {
		super (expt);

		populations = new HashMap<Class<? extends Model>, List<Model>> ();
		for (Class<? extends Model> theory : expt.getTheories ()) {
			populations.put (theory, new ArrayList<Model> ());
		}
	}

	public String toString () {
		return  "MTMC [non-dominated] " + getCyclesAsString ();
	}

  /**
   * Returns a list of models from given theory which are non-dominated within 
   * the given theory.
   */
	public List<Model> extractLocalNonDominatedModels (Class<? extends Model> theory) {
		List<Model> models = new ArrayList<> ();
		separateNonDominatedModels (getPopulation (theory), models);

		return models;
	}

  /**
   * Returns a list of models from give theory which are non-dominated across 
   * all competing theories.
   */
	public List<Model> extractGlobalNonDominatedModels (Class<? extends Model> theory) {
		List<Model> models = new ArrayList<> ();
		
		for (Model model : getNonDominatedModels ()) {
			if (theory == model.getClass ()) {
				models.add (model);
			}
		}

		return models;
	}
	
	public void create (int size) {
		for (Class<? extends Model> theory : expt.getTheories ()) {
      createPopulationRandomModels (size, populations.get (theory), theory);
    }
  }

	/**
	 * @param numberSets The number of sets to use for the non-dominated 
   * sort.  numberSets must be a positive integer. 
	 * @throws IllegalArgumentException if numberSets or mutate are out of range.
	 */
  public void evolve (int numberSets, int mutate) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
    evolve (
        numberSets,
        mutate,
        new Creator () {
          public void create (List<List<Model>> sets, int mutate)
              throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
            for (Class<? extends Model> theory : expt.getTheories ()) {
              createNewPopulation (populations.get(theory), theory, sets, mutate);
            }
          }
        });
  } 

	public List<Model> getPopulation (Class<? extends Model> theory) { 
		return populations.get (theory); 
	}

	public void sortByConstraint (Class<? extends Model> theory, Constraint constraint) {
		java.util.Collections.sort (populations.get (theory),
				new ModelFitnessComparator (expt, constraint));
		setChanged ();
	}

	protected List<Model> getModels () {
		List<Model> models = new ArrayList<> ();
		for (Class<? extends Model> theory : expt.getTheories ()) {
			models.addAll (populations.get (theory));
		}

		return models;
	}
}


