package info.peterlane.mdk.optimisation.algorithms;

import java.util.*;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;
import info.peterlane.mdk.optimisation.algorithms.comparators.ModelFitnessComparator;

/**
 * Optimisation algorithm for a single theory and multiple criteria, using 
 * non-dominated sorting to rank the models.  Methods allow the 
 * non-dominated models to be retrieved for analysis.
 *
 * @author Peter Lane
 */
public class SingleTheoryMultipleConstraintsND extends MultipleCriteriaND {
	private final List<Model> population;

	// data maintained for observers / analysis
	private Class<? extends Model> theory;

	public SingleTheoryMultipleConstraintsND (Experiment expt) {
		super (expt);

		population = new ArrayList<Model> ();
	}

	public String toString () {
		if (theory == null) return "NO POPULATION";

		return  "STMC [non-dominated] (" + Theory.classNameFor(theory) + ") " + getCyclesAsString ();
	}

	public void create (int size) {
    createPopulationRandomModels (size, population, theory);
  }

  public void evolve (int numberSets, int mutate) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
    evolve (
        numberSets, 
        mutate, 
        new Creator () {
          public void create (List<List<Model>> sets, int mutate) 
              throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
            createNewPopulation (population, theory, sets, mutate);
          }
    });
  }

	public void setTheory (Class<? extends Model> theory) {
		this.theory = theory;
	}

	public List<Model> getPopulation () { return population; }

	public void sortByConstraint (Constraint constraint) {
		java.util.Collections.sort (population,
				new ModelFitnessComparator (expt, constraint));
		setChanged ();
	}

	protected List<Model> getModels () {
		List<Model> models = new ArrayList<Model> ();
		models.addAll (population);
		return models;
	}


}


