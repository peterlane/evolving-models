package info.peterlane.mdk.optimisation.algorithms.comparators;

import java.util.Comparator;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/** Provide a comparator to sort models based on summed value of given constraints. */
public class ModelSummedFitnessComparator implements Comparator<Model> {
	private final Experiment expt;
	
	public ModelSummedFitnessComparator (Experiment expt) {
		this.expt = expt;
	}

	/** Put the best performing models at the beginning of the sort. */
	public int compare (Model lhs, Model rhs) {
		double lhsResult = summedFitness (lhs);
		double rhsResult = summedFitness (rhs);

		if (lhsResult > rhsResult) return +1;
		if (rhsResult > lhsResult) return -1;
		return 0;
	}

	private double summedFitness (Model model) {
		double total = 0.0;
		for (Constraint constraint : expt.getConstraints()) {
			total += constraint.computeMatch (expt.evaluate (model));
		}
		return total;
	}
}

