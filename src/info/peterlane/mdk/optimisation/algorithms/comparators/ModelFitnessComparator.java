package info.peterlane.mdk.optimisation.algorithms.comparators;

import java.util.Comparator;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/** Provide a comparator to sort models based on value of a single constraint. */
public class ModelFitnessComparator implements Comparator<Model> {
	private final Constraint constraint;
	private final Experiment expt;
	
	public ModelFitnessComparator (Experiment expt, Constraint constraint) {
		this.constraint = constraint;
		this.expt = expt;
	}

	/** Put the best performing models at the beginning of the sort. */
	public int compare (Model lhs, Model rhs) {
		double lhsResult = constraint.computeMatch (expt.evaluate (lhs));
		double rhsResult = constraint.computeMatch (expt.evaluate (rhs));

		if (lhsResult > rhsResult) return +1;
		if (rhsResult > lhsResult) return -1;
		return 0;
	}
}

