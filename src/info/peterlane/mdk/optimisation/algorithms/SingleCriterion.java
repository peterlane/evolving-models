package info.peterlane.mdk.optimisation.algorithms;

import java.util.ArrayList;
import java.util.List;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

/**
* Supports an optimisation algorithm managing a single optimisation 
* criterion.  Class manages the population and a list of the 
* best models.
*
* @author Peter Lane
*/
abstract public class SingleCriterion extends GeneticAlgorithm {

	protected final List<Model> population;
	protected final Experiment expt;
	protected Class<? extends Model> theory;
	protected final List<Model> bestModels;

	protected SingleCriterion (Experiment expt) throws IllegalArgumentException {
    if (expt.getTheories().isEmpty ()) {
      throw new IllegalArgumentException ("No theories in experiment");
    }

		this.expt = expt;
		theory = expt.getTheories().get (0);
		population = new ArrayList<Model> ();
		bestModels = new ArrayList<Model> ();
	}
 
  protected SingleCriterion (Experiment expt, Class<? extends Model> theory) {
		this.expt = expt;
    this.theory = theory;
		population = new ArrayList<Model> ();
		bestModels = new ArrayList<Model> ();
	}
	
	public void create (int size) {
		population.clear ();
		clearCollectedData ();
		for (int i = 0; i < size; ++i) {
      try {
        population.add (theory.getDeclaredConstructor().newInstance ());
      } catch (IllegalAccessException | InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException ie) {
        ; // ignore
      }
		}
		sortPopulation ();
		setChanged ();
	}

	/**
	 * @param elite The proportion of elite individuals to retain in each cycle,
	 * which must be a positive integer less than 100.
	 * @throws IllegalArgumentException if elite or mutate are out of range.
	 */
	public void evolve (int elite, int mutate) 
      throws IllegalArgumentException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
		
		if (population.isEmpty ()) { return; }

		if (elite < 0 || elite >= 100) {
			throw new IllegalArgumentException ("Invalid elite parameter to evolve");
		}

		if (mutate < 0 || mutate > 100) {
			throw new IllegalArgumentException ("Invalid mutate parameter to evolve");
		}

		int populationSize = population.size (); // remember target size of population

		updateCollectedData ();
		sortPopulation ();
		List<Model> eliteIndividuals = getEliteModels (elite);

		population.clear ();
		population.addAll (eliteIndividuals);
		createPopulation (
				population,
				eliteIndividuals,
				mutate,
				populationSize);

		sortPopulation ();
		setChanged ();
	}

  public void setTheory (Class<? extends Model> theory) {
    this.theory = theory;
  }

	public List<Model> getPopulation () { return population; }

	/**
	 * Returns the history of the fitness values of the best model in the
	 * evolving population.
	 */
	abstract public List<Double> getFitnessValuesHistory ();

	/**
	 * Returns the elite individuals in current population.
	 *
	 * @param elite The proportion of elite individuals to retain in each cycle,
	 * which must be a positive integer less than 100.
	 * @throws IllegalArgumentException if <code>elite</code> is out of range.
	 */
	public List<Model> getEliteModels (int elite) throws IllegalArgumentException {

		if (elite < 0 || elite >= 100) {
			throw new IllegalArgumentException ("elite proportion invalid");
		}

		List<Model> eliteIndividuals = new ArrayList<> ();
		for (int i = 0, n = elite * population.size () / 100; i < n; ++i) {
			eliteIndividuals.add (population.get (i));
		}

		return eliteIndividuals;
	}

	/**
	 * Confirm if given model is in the elite group, with give proportion.
	 * @param elite The proportion of elite individuals to retain in each cycle,
	 * which must be a positive integer less than 100.
	 * @throws IllegalArgumentException if <code>elite</code> is out of range.
	 */
	public boolean isEliteModel (Model model, int elite) throws IllegalArgumentException {
		return getEliteModels(elite).contains (model);
	}

	private void clearCollectedData () {
		cycles = 0;
		bestModels.clear ();
	}

	private void updateCollectedData () {
		cycles += 1;
		bestModels.add (population.get (0));
	}
	
	abstract protected void sortPopulation ();
}




