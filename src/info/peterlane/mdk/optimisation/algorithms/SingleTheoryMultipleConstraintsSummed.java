package info.peterlane.mdk.optimisation.algorithms;

import java.util.*;

import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;
import info.peterlane.mdk.optimisation.algorithms.comparators.ModelSummedFitnessComparator;

/**
 * Optimisation algorithm for a single theory and multiple criteria, using the 
 * summed fitness of all the constraints to rank the models.  
 *
 * @author Peter Lane
 */
public class SingleTheoryMultipleConstraintsSummed extends SingleCriterion {

	public SingleTheoryMultipleConstraintsSummed (Experiment expt) throws IllegalArgumentException {
		super (expt);
	}

	public SingleTheoryMultipleConstraintsSummed (Experiment expt, Class<? extends Model> theory) throws IllegalArgumentException {
		super (expt, theory);
	}

	public String toString () {
		if (theory == null) return "NO POPULATION";

		return Theory.classNameFor (theory) + " " + getCyclesAsString ();
	}

	public List<Double> getFitnessValuesHistory () { 
		List<Double> results = new ArrayList<> ();

		if (expt != null) {
			for (Model model : bestModels) {
				results.add (getSummedFitness (model));
			}
		}

		return results; 
	}

	public double getSummedFitness (Model model) {
		double total = 0.0;
		for (int i = 0, n = expt.getConstraints().size (); i < n; ++i) {
			total += expt.getConstraints().get(i).computeMatch (expt.evaluate (model));
		}
		return total;
	}

	protected void sortPopulation () {
		java.util.Collections.sort (population,
				new ModelSummedFitnessComparator (expt));
	}
}

