package info.peterlane.mdk.optimisation.gui;

import java.awt.*;
import java.awt.event.*;
import java.awt.Font;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

/** A collection of useful methods for constructing the graphical interface.
  * @author Peter Lane, 14th October 2006
  */
public class Tools {
	private Tools () {} // constructor not needed

	public static JMenuItem makeMenuItem (ActionListener listener, String title, String cmd) {
		JMenuItem item = new JMenuItem (title);
		item.setActionCommand (cmd);
		item.addActionListener (listener);
		
		return item;
	}
	
	public static JMenuItem makeMenuItem (
			ActionListener listener, 
			String title, 
			char mnemonic, 
			String cmd) {
		JMenuItem item = makeMenuItem (listener, title, cmd);
		if (mnemonic != ' ') { item.setMnemonic (mnemonic); }
		return item;
	}

	public static JMenuItem makeMenuItem (
			ActionListener listener, 
			String title, 
			String iconName, 
			String cmd) {
		JMenuItem item = new JMenuItem (title, makeIcon (iconName));
		item.setActionCommand (cmd);
		item.addActionListener (listener);
		return item;
	}	
	
	public static JMenuItem makeMenuItem (
			ActionListener listener, 
			String title, 
			String iconName, 
			char mnemonic, 
			String cmd) {
		JMenuItem item = makeMenuItem (listener, title, iconName, cmd);
		if (mnemonic != ' ') { item.setMnemonic (mnemonic); }

		return item;
	}

	private static MouseAdapter makeMouseAdapter (final JLabel feedback, final String helpMsg) {
		return new MouseAdapter () {
			public void mousePressed (MouseEvent e) {
				feedback.setText (" "); // clear when menu selected
			}
			public void mouseClicked (MouseEvent e) {}
			public void mouseReleased (MouseEvent e) {}
			public void mouseEntered (MouseEvent e) {
				feedback.setText (helpMsg);
			}
			public void mouseExited (MouseEvent e) {
				feedback.setText (" ");
			}
		};
	}

	public static JMenuItem makeMenuItemWithFeedback (
			ActionListener listener,
			JLabel feedback,
			String helpMsg,
			String title,
			String iconName,
			char mnemonic,
			String cmd) {
		JMenuItem item = makeMenuItem (listener, title, iconName, mnemonic, cmd);
		item.addMouseListener (makeMouseAdapter (feedback, helpMsg));
		return item;
	}

	public static JMenuItem makeMenuItemWithFeedback (
			ActionListener listener,
			final JLabel feedback,
			final String helpMsg,
			String title,
			char mnemonic,
			String cmd) {
		JMenuItem item = makeMenuItem (listener, title, mnemonic, cmd);
		item.addMouseListener (makeMouseAdapter (feedback, helpMsg));
		return item;
	}

	public static JMenuItem makeMenuItemWithFeedback (
			ActionListener listener,
			final JLabel feedback,
			final String helpMsg,
			String title,
			String cmd) {
		JMenuItem item = makeMenuItem (listener, title, cmd);
		item.addMouseListener (makeMouseAdapter (feedback, helpMsg));
		return item;
	}

	public static Icon makeIcon (String name) {
		return new ImageIcon (Tools.class.getResource("icons/" + name));
	}

	public static JButton makeButton (ActionListener listener, 
			String title, 
			String commandName,
			String tooltip) {
		JButton button = new JButton (title);
		button.setToolTipText (tooltip);
		button.setActionCommand (commandName);
		button.addActionListener (listener);
	
		return button;
	}

	public static String makeFloat (double number) {
		return String.format ("%2.2f", number);
	}

	public static JLabel makeValueLabel (String title) {
		JLabel label = new JLabel (title, SwingConstants.CENTER);
		label.setFont (label.getFont().deriveFont (Font.PLAIN));

		return label;
	}

	/** Change the label to show two decimal places of a given value,
	  * but the tooltip shows the full number.
	  */
	public static void changeLabel (JLabel label, double value) {
		label.setText (makeFloat (value));
		label.setToolTipText ("" + value);
	}
	
	public static void selectInternalFrame (JInternalFrame f) {
		try {
			if (f.isIcon ()) {
				f.setIcon (false);
			}
			f.setSelected (true);
		} catch (java.beans.PropertyVetoException pve) {
			// Don't worry if the selection action is vetoed
		}
	}

	public static double getFieldValue (JFormattedTextField field) {
		Object number = field.getValue ();
		if (number instanceof Long) {
			return ((Long)number).doubleValue ();
		} else { // (number instanceof Double)
			return (Double)number;
		}
	}

	public static JPanel makeTwoLabelledControls (String title1, String tooltip1, Component component1,
			String title2, String tooltip2, Component component2) {
		JLabel label1 = new JLabel (title1, SwingConstants.RIGHT);
		JLabel label2 = new JLabel (title2, SwingConstants.RIGHT);
		if (label1.getPreferredSize().getWidth () > label2.getPreferredSize().getWidth ()) {
			label2.setPreferredSize (label1.getPreferredSize ());
		} else {
			label1.setPreferredSize (label2.getPreferredSize ());
		}
		
		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (makeLabelledControl (label1, tooltip1, component1));
		panel.add (makeLabelledControl (label2, tooltip2, component2));

		return panel;
	}

	public static JPanel makeLabelledControl (JLabel label, String tooltip, Component component) {
		label.setToolTipText (tooltip);
		JPanel panel = new JPanel (new BorderLayout ());
		panel.setBorder (new EmptyBorder (3, 3, 3, 3));
		panel.add (label, BorderLayout.WEST);
		panel.add (component);

		return panel;
	}

	public static void addStandardRenderers (JTable table) {
		table.setDefaultRenderer (String.class,
			new Tools.TableStringRenderer ());
		table.setDefaultRenderer (double.class,
			new Tools.TableDoubleRenderer ());
	}
	
	public static void addStandardRenderers (JTable table, TableCellHighlighter highlighter) {
		table.setDefaultRenderer (String.class,
			new Tools.TableStringRenderer (highlighter));
		table.setDefaultRenderer (double.class,
			new Tools.TableDoubleRenderer (highlighter));
	}

	private static class TableStringRenderer extends JLabel implements TableCellRenderer {
		private final TableCellHighlighter _highlighter;

		public TableStringRenderer () {
			this (null);
		}
		
		public TableStringRenderer (TableCellHighlighter highlighter) { 
			super ("", SwingConstants.CENTER);
			
			setOpaque (true); 
			_highlighter = highlighter;
		}

		public Component getTableCellRendererComponent(JTable table, Object value, 
				boolean isSelected, boolean hasFocus, int row, int column) {
			setBackground ( (isSelected) ? table.getSelectionBackground () : Color.WHITE);
			if (_highlighter != null && _highlighter.isCellHighlighted (row, column)) {
				setForeground (Color.BLUE);
			} else {
				setForeground (Color.BLACK);
			}
			setFont (getFont().deriveFont (Font.PLAIN));
			setText (value.toString());
			return this;
		}
	}

	private static class TableDoubleRenderer extends JLabel implements TableCellRenderer {
		private final TableCellHighlighter _highlighter;

		public TableDoubleRenderer () {
			this (null);
		}
		
		public TableDoubleRenderer (TableCellHighlighter highlighter) { 
			super ("", SwingConstants.CENTER);
			
			setOpaque (true); 
			_highlighter = highlighter;
		}

		public Component getTableCellRendererComponent(JTable table, Object value, 
				boolean isSelected, boolean hasFocus, int row, int column) {
			setBackground ( (isSelected) ? table.getSelectionBackground () : Color.WHITE);
			setFont (getFont().deriveFont (Font.PLAIN));
			if (_highlighter != null && _highlighter.isCellHighlighted (row, column)) {
				setForeground (Color.BLUE);
			} else {
				setForeground (Color.BLACK);
			}
			setToolTipText (value.toString ());
			if (value instanceof Double) {
				setText (Tools.makeFloat ((Double)value));
			} else {
				setText (""); // can happen if no number stored
			}
			return this;
		}
	}

	public interface TableCellHighlighter {
		public boolean isCellHighlighted (int r, int c);
	}

	public static JComboBox<String> createTheorySelector (Experiment expt) {
		JComboBox<String> theorySelector = new JComboBox<String> ();
		for (Class<? extends Model> theory : expt.getTheories ()) {
			theorySelector.addItem (Theory.classNameFor (theory));
		}
		return theorySelector;
	}

  /**
   * Use reflection to see if theory has a static method 'getIcon'.  
   * If so, call it to retrieve the icon, else return a help icon.
   */
  public static Icon getIcon (Class<? extends Model> theory) {
    try {
      Method m = theory.getMethod ("getIcon");
      return (Icon)(m.invoke (null));
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      ; // ignore, so following statement done by default
    }
    return Tools.makeIcon("Help16.gif");
  }
}

