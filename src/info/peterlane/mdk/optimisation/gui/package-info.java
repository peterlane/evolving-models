/**
 * A simple Graphical User Interface application for running 
 * a simulation experiment.  Caller class must provide an 
 * Experiment to instantiate the Shell.  The application 
 * provides views to create and experiment with different 
 * models, run the different optimisation algorithms, and 
 * access some visualisations to analyse the results.
 *
 * @author Peter Lane
 */
package info.peterlane.mdk.optimisation.gui;
