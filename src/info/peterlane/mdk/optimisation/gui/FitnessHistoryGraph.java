package info.peterlane.mdk.optimisation.gui;

import java.awt.Color;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;

import info.peterlane.mdk.optimisation.algorithms.SingleCriterion;
import info.peterlane.mdk.optimisation.gui.graphs.Line;
import info.peterlane.mdk.optimisation.gui.graphs.LineGraph;

public class FitnessHistoryGraph extends JInternalFrame {
	private final SingleCriterion population;

	public FitnessHistoryGraph (String title, SingleCriterion population) {
		super ("Fitness: " + title, true, true, true, true);

		this.population = population;

		setContentPane (new LineGraph (getLine (), "Cycle", "Fitness"));

		setSize (250, 200);
		setVisible (true);
	}

	private Line getLine () {
		return new Line (population.getFitnessValuesHistory (), Color.BLUE);
	}
}

