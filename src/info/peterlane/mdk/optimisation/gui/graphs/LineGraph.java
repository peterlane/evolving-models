package info.peterlane.mdk.optimisation.gui.graphs;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.util.ArrayList;
import javax.swing.*;

public class LineGraph extends GraphPanel {
	private Line [] lines;
	private boolean[] marks; // vertical lines for x = constant
	
	public LineGraph (Line line, String horizontalAxisLabel, String verticalAxisLabel) {
		this (new Line[]{line}, horizontalAxisLabel, verticalAxisLabel);
	}

	public LineGraph (Line[] lines, String horizontalAxisLabel, String verticalAxisLabel) {
		super (horizontalAxisLabel, verticalAxisLabel);
		this.lines = lines;
		this.marks = null;
	}

	public LineGraph (ArrayList given_lines, String horizontalAxisLabel, String verticalAxisLabel) {
		super (horizontalAxisLabel, verticalAxisLabel);
		this.lines = new Line[given_lines.size ()];
		for (int i = 0; i < given_lines.size (); ++i) {
			this.lines[i] = (Line)given_lines.get (i);
		}
		this.marks = null;
	}

	public void setMarkedPoints (boolean[] marks) {
		this.marks = marks;
		repaint ();
	}

	double getMaximumX () {
		double result = 0;
		for (int l=0, m=lines.length; l<m; ++l) {
			if (lines[l].getPoints().length > result) {
				result = lines[l].getPoints().length;
			}
		} 

		return java.lang.Math.ceil(result+1);
	}
	
	double getMaximumY () {
		double result = 0;
		double[] points;
		for (int l=0, m=lines.length; l<m; ++l) {
			points = lines[l].getPoints ();
			for (int i=0, n = points.length; i<n; ++i) {
				if (points[i] > result) {
					result = points[i];
				}
			}
		}

		return result;
	}
	
	void drawGraphLine (Graphics2D g2, Line line) {
		double[] points = line.getPoints();
		if (points.length == 0) return; // no points to draw

		int numX = points.length; // assume numX > 1
		int x, y;

		g2.setColor(line.getColour());

		// draw the dots
		for (int i=0, n=points.length; i<n; ++i) {
			x = axisLeft + (int)(i * scaleX);
			y = axisBase - (int)(points[i] * scaleY);
			g2.fillOval (x-pointRadius, y-pointRadius, 2*pointRadius, 2*pointRadius);
		}
		// join the dots
		int lastX = axisLeft;
		int lastY = axisBase - (int)(points[0] * scaleY);
		for (int i=1, n=points.length; i<n; ++i) {
			x = axisLeft + (int)(i * scaleX);
			y = axisBase - (int)(points[i] * scaleY);
			g2.drawLine (lastX, lastY, x, y);
			lastX = x;
			lastY = y;
		}
		// draw the deviations
		if (line.hasDeviations ()) {
			double[] deviations = line.getDeviations ();
			int deviation;
			g2.setColor (Color.BLACK);
			for (int i=0, n=points.length; i<n; ++i) {
				x = axisLeft + (int)(i * scaleX);
				y = axisBase - (int)(points[i] * scaleY);
				deviation = (int)(deviations[i] * scaleY);
				if (deviation > 0) {
					g2.drawLine (x, y-deviation, x, y+deviation);
					g2.drawLine (x-2, y-deviation, x+2, y-deviation);
					g2.drawLine (x-2, y+deviation, x+2, y+deviation);
				}
			}
		}
	}

	private void drawMarkedPoints (Graphics2D g2) {
		if (marks == null) { return; }
		int x;

		for (int i=0, n=marks.length; i<n; ++i) {
			if (marks[i]) {
				x = axisLeft + (int)(i * scaleX) - pointRadius;
				g2.setColor(Color.RED);
				g2.drawLine (x, axisBase + 18, x, axisTop);
			}
		}
	}

	public void paint (Graphics g) {
		super.paint (g);

		Graphics2D g2 = (Graphics2D)g;
		if (canDrawGraph()) {
			for (int i=0, n=lines.length; i<n; ++i) {
				drawGraphLine (g2, lines[i]);
			}
			drawMarkedPoints (g2);
		}
	}
}

