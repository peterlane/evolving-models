package info.peterlane.mdk.optimisation.gui.graphs;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import javax.swing.*;

import info.peterlane.mdk.optimisation.gui.Tools;

public abstract class GraphPanel extends JPanel {
	protected String horizontalAxisLabel, verticalAxisLabel;
	final int pointRadius = 1;
	
	public GraphPanel (String horizontalAxisLabel, String verticalAxisLabel) {
		super (true); // enable double buffering
		this.horizontalAxisLabel = horizontalAxisLabel;
		this.verticalAxisLabel = verticalAxisLabel;
	}

	abstract double getMaximumX ();
	abstract double getMaximumY ();
	
	int width,
	    height,
	    verticalLabelX,
	    axisLeft,
	    axisRight,
	    axisTop,
	    axisBase,
	    xLabelsHeight;
	float scaleX, scaleY;
	final float 
		axisLabelFontHeight = 10,
		axisMainLabelFontHeight = 12,
		axisLabelGap = 3;
	final Font
		axisFont = new Font ("SanSerif", Font.PLAIN, 10),
		axisLabelFont = new Font ("SanSerif", Font.BOLD, 12);
	
	double getStringWidth (String str, Graphics2D g2, Font font) {
		TextLayout tl = new TextLayout (str, font, g2.getFontRenderContext());
		return tl.getBounds().getWidth();
	}

	int rescaleSizeX (double x) {
		return (int)(scaleX * x);
	}

	int rescaleX (double x) {
		return axisLeft + rescaleSizeX(x);
	}

	int rescaleSizeY (double y) {
		return (int)(scaleY * y);
	}
	
	int rescaleY (double y) {
		return axisBase - rescaleSizeY(y);
	}

	private double getWidestLabelWidth (Graphics2D g2) {
		double maxW = 0.0;
		double w;
		for (int i=0; i<labels.length; ++i) {
			w = getStringWidth (labels[i], g2, axisFont);
			if (w > maxW) { maxW = w; }
		}
		return maxW;
	}
	
	private int computeHorizontalLabelHeight (Graphics2D g2) {
		// need to check if own labels provided, and they need to be rotated
		if (useProvidedLabels) {
			double width = getWidestLabelWidth (g2);
			if (width > (rescaleX(1)-rescaleX(0))) { // too wide, so rotate 
				return (int)width; // rotated height is its width
			}
		}
		return (int)axisLabelFontHeight; 
	}

	private void recomputeDimensions (Graphics2D g2) {
		width = getWidth ();
		height = getHeight ();
		verticalLabelX = 5 + (int)axisMainLabelFontHeight;
		axisLeft = verticalLabelX + (int)axisMainLabelFontHeight + (int)axisLabelGap +
			(int)getStringWidth(Tools.makeFloat (getMaximumY()), g2, axisFont);
		axisRight = width - 5;
		axisTop = 5;
		axisBase = height - computeHorizontalLabelHeight(g2) - 10 - (int)axisMainLabelFontHeight;
		xLabelsHeight = axisBase + computeHorizontalLabelHeight(g2) + 
			7 + (int)axisMainLabelFontHeight;
		scaleX = (float)(axisRight - axisLeft) / Math.max (1, (float)getMaximumX());
		scaleY = ((float) (axisBase - axisTop)) / Math.max (1, (float)getMaximumY());
	}

	private boolean  useProvidedLabels = false;
	private boolean  centerLabels      = false;
	private String[] labels            = new String[] {};
	public void setProvidedLabels (String[] labels, boolean center) {
		useProvidedLabels = true;
		centerLabels = center;
		this.labels = labels;  // assume labels.length == getMaximumX ()
		repaint ();
	}

	private void drawHorizontalAxis (Graphics2D g2) {
		g2.setColor (Color.BLACK);
		g2.drawLine (axisLeft, axisBase, axisRight, axisBase);
		// add axis label
		int horizontalLabelX = (int)(axisLeft + axisRight - 
				getStringWidth (horizontalAxisLabel, g2, axisLabelFont))/2;
		g2.setFont (axisLabelFont);
		g2.drawString (horizontalAxisLabel, horizontalLabelX, xLabelsHeight);
		// add tick marks and labels, spacing so font fits
		g2.setFont (axisFont);
		String label;
		double labelWidth;
		int xCoord;
		float xLabel;
		float xLastLabel = (float)axisRight;

		for (double i = Math.max (1, getMaximumX ()), n = i; i > 0; i -= n/10) {
			label = Tools.makeFloat (i);
			labelWidth = getStringWidth (label, g2, axisFont);
			xCoord = rescaleX(i);
			xLabel = xCoord - (float)(labelWidth/2);
			if (xLabel < xLastLabel) {
				g2.drawString (label, xLabel, axisBase + 18);
				g2.drawLine (xCoord, axisBase, xCoord, axisBase + 5);
				xLastLabel = xLabel - (float)labelWidth - axisLabelGap;
			}
		}

	}

	private void drawVerticalAxis (Graphics2D g2) {
		g2.setColor (Color.BLACK);
		g2.drawLine (axisLeft, axisBase, axisLeft, axisTop);
		drawVerticalAxisLabel (g2);
		// add tick marks and labels, spacing so font fits
		g2.setFont (axisFont);
		String label;
		int yCoord;
		float yLabel;
		float yLastLabel = (float)axisTop;

		// try to put 10 labels on vertical axis, starting from top
		for (double i = Math.max (1, getMaximumY ()), n = i; i > 0; i -= n/10) {
			label = Tools.makeFloat (i);
			yCoord = rescaleY (i);
			yLabel = yCoord + (axisLabelFontHeight/2);
			if (yLabel > yLastLabel) {
				g2.drawString (label, axisLeft - 7 - 
						(float)getStringWidth(label, g2, axisFont),
						yLabel);
				g2.drawLine (axisLeft-5, yCoord, axisLeft, yCoord);
				yLastLabel = yLabel + axisLabelFontHeight + axisLabelGap;
			}
		}
	}

	private void drawVerticalAxisLabel (Graphics2D g2) {
		int verticalLabelY = (int) (axisBase + axisTop + 
				getStringWidth (verticalAxisLabel, g2, axisLabelFont))/2;
		g2.setFont (axisLabelFont);
		AffineTransform saveAT = g2.getTransform();
		g2.translate (verticalLabelX, verticalLabelY);
		g2.rotate (Math.PI*1.5);
		g2.drawString (verticalAxisLabel, 0, 0); 
		g2.setTransform (saveAT);
	}

	boolean canDrawGraph () {
		return (width >= 100 && height >= 100);
	}

	public void paint (Graphics g) {
		super.paint (g); // make sure the background of the JPanel is drawn

		Graphics2D g2 = (Graphics2D)g;

		g2.setBackground (Color.WHITE);
		g2.clearRect (0, 0, this.getWidth (), this.getHeight ());

		recomputeDimensions (g2); // make sure sizes are upto date
		if (canDrawGraph()) {
			drawHorizontalAxis (g2);
			drawVerticalAxis (g2);
		} else {
			g2.drawString ("Graph too small", 10, 20);
		}
	}
}

