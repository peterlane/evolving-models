package info.peterlane.mdk.optimisation.gui.graphs;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

public class ConstraintGraph extends GraphPanel {
	private final List<Model> models;
	private Experiment expt;
	private Constraint constraint1, constraint2;

	public ConstraintGraph (List<Model> models, Experiment expt, 
			Constraint constraint1, Constraint constraint2) {
		super (constraint2.toString (), constraint1.toString ());
		
		this.models = models;
		this.expt = expt;
		this.constraint1 = constraint1;
		this.constraint2 = constraint2;
	}

	double getMaximumX () { return getMaximum (constraint2); }
	double getMaximumY () { return getMaximum (constraint1); }

	private double getMaximum (Constraint constraint) {
		double result = 0;
		for (Model model : models) {
			if (getValue (constraint, model) > result) {
				result = getValue (constraint, model);
			}
		}
		
		return result; 
	}

	private double getXValue (Model model) { return getValue (constraint2, model); }
	private double getYValue (Model model) { return getValue (constraint1, model); }
	private double getValue (Constraint constraint, Model model) {
		return constraint.computeMatch (expt.evaluate (model));
	}

	public void paint (Graphics g) {
		super.paint (g); // paint background and axes

		Graphics2D g2 = (Graphics2D)g;
		if (canDrawGraph ()) {
			g2.setColor (Color.BLUE);
			for (Model model : models) {
				int x = rescaleX (getXValue (model));
				int y = rescaleY (getYValue (model));
				g2.fillOval (x-pointRadius, y-pointRadius, 2*pointRadius, 2*pointRadius);
			}
		}
	}

	/** The first parameter indexes the vertical axis label, the second parameter indexes 
	  * the horizontal axis label.
	  */
	public void update (Constraint constraint1, Constraint constraint2) {
		this.constraint1 = constraint1;
		this.constraint2 = constraint2;
		verticalAxisLabel = constraint1.toString ();
		horizontalAxisLabel = constraint2.toString ();

		repaint ();
	}
}
