package info.peterlane.mdk.optimisation.gui.graphs;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import info.peterlane.mdk.optimisation.Model;

public class ParameterGraph extends GraphPanel {
	private final List<Model> models;
	private int parameter1, parameter2;

	public ParameterGraph (List<Model> models, int parameter1, int parameter2) {
		super (
        models.get(0).getParameterName(parameter2), 
        models.get(0).getParameterName(parameter1));
		this.models = models;
		this.parameter1 = parameter1;
		this.parameter2 = parameter2;
	}

	double getMaximumX () { return getMaximum (parameter2); }
	double getMaximumY () { return getMaximum (parameter1); }

	private double getMaximum (int parameter) {
		double result = 0;
		for (Model model : models) {
			if (model.getParameter (parameter) > result) {
				result = model.getParameter (parameter);
			}
		}

		return result; 
	}

	public void paint (Graphics g) {
		super.paint (g); // paint background and axes

		Graphics2D g2 = (Graphics2D)g;
		if (canDrawGraph ()) {
			g2.setColor (Color.BLUE);
			for (Model model : models) {
				int x = rescaleX (model.getParameter (parameter2));
				int y = rescaleY (model.getParameter (parameter1));
				g2.fillOval (x-pointRadius, y-pointRadius, 2*pointRadius, 2*pointRadius);
			}
		}
	}

	/** The first parameter indexes the vertical axis label, the second parameter indexes 
	  * the horizontal axis label.
	  */
	public void update (int parameter1, int parameter2) {
		this.parameter1 = parameter1;
		this.parameter2 = parameter2;
		verticalAxisLabel = models.get(0).getParameterName(parameter1);
		horizontalAxisLabel = models.get(0).getParameterName(parameter2);

		repaint ();
	}
}
