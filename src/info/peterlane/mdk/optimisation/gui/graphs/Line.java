package info.peterlane.mdk.optimisation.gui.graphs;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Line {
	double[] points;
	double[] deviations;
	Color colour;

	public Line (double[] points, Color colour) {
		this.points = points;
		this.deviations = null;
		this.colour = colour;
	}

	public Line (List given_points) {
		this.points = new double[given_points.size ()];
		for (int i = 0; i < given_points.size (); ++i) {
			this.points[i] = ((Double)given_points.get (i)).doubleValue ();
		}
		this.deviations = null;
		this.colour = Color.BLACK;
	}
			
	public Line (List given_points, Color colour) {
		this.points = new double[given_points.size ()];
		for (int i = 0; i < given_points.size (); ++i) {
			this.points[i] = ((Double)given_points.get (i)).doubleValue ();
		}
		this.deviations = null;
		this.colour = colour;
	}

	public Line (double[] points, double[] deviations, Color colour) {
		this.points = points;
		this.deviations = deviations;
		this.colour = colour;
	}

	public double[] getPoints () { 
		return points;
	}

	// Assume i within range
	public double getPoint (int i) {
		return points[i];
	}

	public boolean hasDeviations () {
		return (deviations != null);
	}

	public double[] getDeviations () {
		return deviations;
	}
	
	public Color getColour () {
		return colour;
	}
}
