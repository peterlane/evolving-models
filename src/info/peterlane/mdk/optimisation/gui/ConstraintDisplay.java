package info.peterlane.mdk.optimisation.gui;

import java.awt.*;
import java.awt.event.*;

import java.util.List;
import javax.swing.*;

import info.peterlane.mdk.optimisation.gui.graphs.ConstraintGraph;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

public class ConstraintDisplay extends JInternalFrame implements ActionListener {
	private JComboBox<Constraint> constraintX, constraintY;
	private ConstraintGraph graph;

	public ConstraintDisplay (String title, List<Model> models, Experiment expt) {
		super ("Constraints: " + title, true, true, true, true);

		if (models.isEmpty ()) {
			add (new JLabel ("No models to display"));
		} else {
			add (createSelectors (expt), BorderLayout.NORTH);
			add (createDisplay (models, expt));
			add (new JLabel ("Results for: " + models.size() + 
						" model" + (models.size () == 1 ? "" : "s")),
					BorderLayout.SOUTH);
		}

		setMinimumSize (new Dimension (450, 250));
		setSize (450, 300);
		setVisible (true);
	}

	// assume that there is at least one model
	private JPanel createSelectors (Experiment expt) {
		constraintX = new JComboBox<Constraint> (expt.getConstraints().toArray (new Constraint[expt.getConstraints().size()]));
		constraintX.addActionListener (this);
		constraintY = new JComboBox<Constraint> (expt.getConstraints().toArray (new Constraint[expt.getConstraints().size()]));
		constraintY.addActionListener (this);

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.X_AXIS));

		panel.add (Tools.makeLabelledControl (new JLabel ("Constraint 1: "), 
					"constraint for Y-axis",
					constraintY));
		panel.add (Tools.makeLabelledControl (new JLabel ("Constraint 2: "), 
					"constraint for X-axis",
					constraintX));

		return panel;
	}

	private JPanel createDisplay (List<Model> models, Experiment expt) {
		graph = new ConstraintGraph (models, expt, getConstraintY (), getConstraintX ());
		
		JPanel panel = new JPanel (new GridLayout (1, 1));
		panel.add (graph);

		return panel;
	}

	private Constraint getConstraintX () { return (Constraint)constraintX.getSelectedItem (); }
	private Constraint getConstraintY () { return (Constraint)constraintY.getSelectedItem (); }
	
	public void actionPerformed (ActionEvent e) {
		graph.update (getConstraintY (), getConstraintX ());
	}
}

