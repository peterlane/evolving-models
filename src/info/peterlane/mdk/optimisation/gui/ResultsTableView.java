package info.peterlane.mdk.optimisation.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.models.ModelView;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.algorithms.comparators.ModelFitnessComparator;

/** Display a table of results achieved by a set of models on all the available constraints.
  */
public class ResultsTableView extends JInternalFrame {
	private final Shell shell;
	private final Experiment expt;
	private final List<Model> models;
	
	public ResultsTableView (String title, List<Model> models, Shell shell, Experiment expt) {
		super ("Results: " + title, true, true, true, true);

		this.shell = shell;
		this.expt = expt;
		this.models = models;

		setLayout (new BorderLayout ());

		if (models.isEmpty ()) {
			add (new JLabel ("No models to display"));
		} else {
			add (new JScrollPane (createTable ()));
			add (new JLabel ("Results for: " + models.size() + 
						" model" + (models.size () == 1 ? "" : "s")),
					BorderLayout.SOUTH);
		}

		setMinimumSize (new Dimension (350, 250));
		setSize (400, 300);
		setVisible (true);
	}

	private void sortByConstraint (Constraint constraint) {
		java.util.Collections.sort (models,
				new ModelFitnessComparator (expt, constraint));
	}

	private JTable createTable () {
		ModelResultsView view = new ModelResultsView ();
		JTable table = new JTable (view);
		Tools.addStandardRenderers (table, view);
		table.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
		// make headers use a bold font
		table.getTableHeader().setFont (table.getTableHeader().getFont().deriveFont (Font.BOLD));
		// catch double clicks on the table, to show a selected model
		table.addMouseListener (new TableMouseListener (table));
		// and on the header, to sort a column
		table.getTableHeader().addMouseListener (new HeaderMouseListener (table.getTableHeader (), view));
		
		return table;
	}

	class HeaderMouseListener implements MouseListener {
		private final JTableHeader header;
		private final DefaultTableModel table;

		HeaderMouseListener (JTableHeader header, DefaultTableModel table) {
			this.header = header;
			this.table = table;
		}

		public void mouseClicked (MouseEvent e) {
			if ((e.getClickCount() == 2) && (e.getButton() == MouseEvent.BUTTON1)) {
				final int column = header.columnAtPoint (e.getPoint ());
				if (column != 0) {
					sortByConstraint (expt.getConstraints().get (column - 1));
					table.fireTableDataChanged ();
				}
			}
		}
		public void mouseEntered  (MouseEvent e) {}
		public void mouseExited   (MouseEvent e) {}
		public void mousePressed  (MouseEvent e) {}
		public void mouseReleased (MouseEvent e) {}
	}
		
	class TableMouseListener implements MouseListener {
		private final JTable table;

		TableMouseListener (JTable table) {
			this.table = table;
		}

		public void mouseClicked (MouseEvent e) {
      if ((e.getClickCount() == 2) && (e.getButton() == MouseEvent.BUTTON1)) {
        // double left-button click -- so show that row's model
        shell.addView (new ModelView (
              models.get (table.rowAtPoint(e.getPoint ())),
              expt
              )
            );
      }
		}

		public void mouseEntered  (MouseEvent e) {}
		public void mouseExited   (MouseEvent e) {}
		public void mousePressed  (MouseEvent e) {}
		public void mouseReleased (MouseEvent e) {}
	}

	class ModelResultsView extends DefaultTableModel implements Tools.TableCellHighlighter {
		private double[] bestValues;

		ModelResultsView () {
			super (1 + models.size (), 1 + expt.getConstraints().size ());

			// maintain the best value for each constraint
			bestValues = new double [expt.getConstraints().size ()];

			for (int i = 0, n = expt.getConstraints().size (); i < n; ++i) {
				bestValues[i] = expt.getConstraints().get(i).computeMatch (expt.evaluate (models.get(0)));
				for (int j = 1, m = models.size (); j < m; ++j) {
					double c = expt.getConstraints().get(i).computeMatch (expt.evaluate (models.get(j)));
					if (bestValues[i] > c) bestValues[i] = c;
				}
			}
		}

		public Class getColumnClass (int c) {
			return (c == 0 ? String.class : double.class);
		}

		public int getColumnCount () {
			return 1 + expt.getConstraints().size ();
		}

		public String getColumnName (int c) {
			if (c == 0) {
				return "Model";
			} else { 
				return expt.getConstraints().get (c-1).toString ();
			}
		}

		public int getRowCount () {
			return (1 + models.size ());
		}

		public boolean isCellEditable (int row, int col) {
			return false;
		}

		public Object getValueAt (int r, int c) {
			if (models.size () <= r) {
				if (c == 0) {
					return "Best Value";
				} else {
					return bestValues[c-1];
				}
			}

			Model model = models.get (r);

			if (c == 0) {
				return model.toString ();
			} else { 
				return expt.getConstraints().get(c-1).computeMatch (expt.evaluate (model));
			}
		}

		// implement TableCellHighlighter interface
		public boolean isCellHighlighted (int r, int c) {
			if (models.size () <= r) return true; // highlight bottom row
			if (c == 0) return false;              // do not highlight model names

			double v = expt.getConstraints().get(c-1).computeMatch (expt.evaluate (models.get(r)));
			return (v == bestValues[c-1]);        // highlight best in column
		}
	}
}

