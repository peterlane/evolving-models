package info.peterlane.mdk.optimisation.gui;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

import info.peterlane.mdk.optimisation.gui.graphs.ParameterGraph;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.Model;

public class ParameterDisplay extends JInternalFrame implements ActionListener {
	private final List<Model> models;
	private JComboBox<String> parameter1, parameter2;
	private ParameterGraph graph;

	public ParameterDisplay (String title, List<Model> models) {
		super ("Parameters: " + title, true, true, true, true);

		this.models = models;

		if (models.isEmpty ()) {
			add (new JLabel ("No models to display"));
		} else {
			add (createSelectors (), BorderLayout.NORTH);
			add (createDisplay ());
			add (new JLabel ("Results for: " + models.size() + 
						" model" + (models.size () == 1 ? "" : "s")),
					BorderLayout.SOUTH);
		}

		setMinimumSize (new Dimension (450, 250));
		setSize (450, 300);
		setVisible (true);
	}

	// assume that there is at least one model
	private JPanel createSelectors () {
		if (models == null || models.isEmpty ()) return new JPanel();

		parameter1 = new JComboBox<String> ();
		parameter1.addActionListener (this);
		parameter2 = new JComboBox<String> ();
		parameter2.addActionListener (this);
    for (int i=0; i < models.get(0).getParameterCount (); i += 1) {
      parameter1.addItem (models.get(0).getParameterName (i));
      parameter2.addItem (models.get(0).getParameterName (i));
    }

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.X_AXIS));

		panel.add (Tools.makeLabelledControl (new JLabel ("Parameter 1: "), 
					"parameter for Y-axis",
					parameter1));
		panel.add (Tools.makeLabelledControl (new JLabel ("Parameter 2: "), 
					"parameter for X-axis",
					parameter2));

		return panel;
	}

	private JPanel createDisplay () {
		graph = new ParameterGraph (models, 0, 0);
		
		JPanel panel = new JPanel (new GridLayout (1, 1));
		panel.add (graph);

		return panel;
	}

	public void actionPerformed (ActionEvent e) {
		if (graph != null) {
			graph.update (parameter1.getSelectedIndex (), parameter2.getSelectedIndex ());
		}
	}
}

