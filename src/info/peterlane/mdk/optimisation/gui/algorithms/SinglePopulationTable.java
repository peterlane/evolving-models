package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.algorithms.GeneticAlgorithm;
import info.peterlane.mdk.optimisation.algorithms.SingleCriterion;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.gui.FitnessHistoryGraph;
import info.peterlane.mdk.optimisation.gui.ParameterDisplay;
import info.peterlane.mdk.optimisation.gui.ResultsTableView;
import info.peterlane.mdk.optimisation.gui.Shell;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.gui.models.ModelView;

abstract public class SinglePopulationTable extends JPanel implements ActionListener {
	private static final String
		FITNESS_GRAPH = "Fitness",
		PARAMETERS_DISPLAY = "Parameters",
		RESULTS_TABLE = "Results";
	private String name;
	private Shell shell;
	private Experiment expt;
	private SingleCriterion population;
	private JLabel title;

	public SinglePopulationTable (String name, Shell shell, Experiment expt, SingleCriterion population) {
		super ();

		this.name = name;
		this.shell = shell;
		this.expt = expt;
		this.population = population;

		setLayout (new BorderLayout ());

		JMenuBar showMenu = new JMenuBar ();
		showMenu.setBorder (new EmptyBorder (3, 3, 3, 3));
		showMenu.setAlignmentY (0.5f);

		JMenu show = new JMenu ("Show");
		show.add (Tools.makeMenuItemWithFeedback (this,
					shell.getFeedbackLabel (),
					"Compute results against all constraints of elite models",
					"Table of elite model results", 
					RESULTS_TABLE));
		show.add (Tools.makeMenuItemWithFeedback (this, 
					shell.getFeedbackLabel (), 
					"Show a graph of best fitness over time",
					"Fitness against cycles", 
					FITNESS_GRAPH));
		show.add (Tools.makeMenuItemWithFeedback (this, 
					shell.getFeedbackLabel (),
					"Show range of values taken by pairs of parameters",
					"Parameter values of elite", 
					PARAMETERS_DISPLAY));
		showMenu.add (show);

		title = new JLabel ("Current population: ");
		Box titleBox = Box.createHorizontalBox ();
		titleBox.add (title);
		titleBox.add (Box.createHorizontalGlue ());
		titleBox.add (showMenu);
		add (titleBox, BorderLayout.NORTH);

		PopulationFitnessView populationView = new PopulationFitnessView ();
		JTable fitnessView = new JTable (populationView);
		Tools.addStandardRenderers (fitnessView, populationView);
		// make headers use a bold font
		fitnessView.getTableHeader().setFont (fitnessView.getTableHeader().getFont().deriveFont (Font.BOLD));
		// catch double clicks on the table, to show a selected model
		fitnessView.addMouseListener (new TableMouseListener (fitnessView));

		add (new JScrollPane (fitnessView));
		((GeneticAlgorithm)population).addObserver (populationView);
	}

	abstract Object computeModelValue (Model model);
	abstract int retrieveEliteProportion ();

	public void actionPerformed (ActionEvent e) {
		String cmd = e.getActionCommand ();

		if (cmd == RESULTS_TABLE) {
      try {
        shell.addView (new ResultsTableView (
              name + " " + population.toString (),
              population.getEliteModels (retrieveEliteProportion ()),
              shell,
              expt));
      } catch (IllegalArgumentException iee) {
        ; // ignore any problems
      }
		} else if (cmd == FITNESS_GRAPH) {
			shell.addView (new FitnessHistoryGraph (name + population.toString (), population));
		} else if (cmd == PARAMETERS_DISPLAY) {
      try {
  			shell.addView (new ParameterDisplay (name + " " + population.toString (), 
	  					population.getEliteModels (retrieveEliteProportion ())));
      } catch (IllegalArgumentException iee) {
        ; // ignore any problems
      }
		}
	}

	class TableMouseListener implements MouseListener {
		private final JTable table;

		TableMouseListener (JTable table) {
			this.table = table;
		}

		public void mouseClicked (MouseEvent e) {
			if ((e.getClickCount() == 2) && 
					(e.getButton() == MouseEvent.BUTTON1)) {
				shell.addView (new ModelView (
              population.getPopulation().get (
                table.rowAtPoint(e.getPoint ())), 
              expt
              )
            );
			}
		}
		public void mouseEntered  (MouseEvent e) {}
		public void mouseExited   (MouseEvent e) {}
		public void mousePressed  (MouseEvent e) {}
		public void mouseReleased (MouseEvent e) {}
	}

	class PopulationFitnessView extends DefaultTableModel 
		implements Observer, Tools.TableCellHighlighter {

			private final String[] headers = new String[] { "Model", "Fitness" };

			PopulationFitnessView () {
				super (population.getPopulation().size (), 2);
			}

			public Class getColumnClass (int c) {
				return (c == 0 ? String.class : double.class);
			}

			public int getColumnCount () { 
				return 2;
			}

			public int getRowCount () {
				return (population.getPopulation().size ());
			}

			public boolean isCellEditable (int row, int col) { 
				return false;
			}

			public String getColumnName (int c) {
				return headers[c];
			}

			public Object getValueAt (int r, int c) {
				if (population.getPopulation().size () <= r) return "";
				Model model = population.getPopulation().get (r);

				if (c == 0) {
					return model.toString ();
				} else { // (c == 1)
					return computeModelValue (model);
				}
			}

			// implement Observer interface
			public void update (Observable o, Object arg) {
				title.setText ("Current population (after " + 
					population.getCycles () + 
					" cycle" + 
					(population.getCycles () == 1 ? "" : "s") +
					"): ");
				fireTableDataChanged ();
			}

			// implement TableCellHighlighter interface
			public boolean isCellHighlighted (int r, int c) {
				if (c != 0) return false;
				if (population.getPopulation().size () <= r) return false;
				Model model = population.getPopulation().get (r);

        try {
  				return population.isEliteModel (model, retrieveEliteProportion ());
        } catch (IllegalArgumentException iee) {
          return false; // invalid elite
        }
			}
		}
}
