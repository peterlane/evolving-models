package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.*;
import info.peterlane.mdk.optimisation.algorithms.*;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

abstract public class NonDominatedGeneticAlgorithmView extends GeneticAlgorithmView {
	private SpinnerNumberModel 
		_ndSets;	// number of sets of non-dominated models to create

	public NonDominatedGeneticAlgorithmView (String title, Shell shell, 
			Experiment expt) {
		super (title, shell, expt);
	}

	// accessors to values
	int getNumberSets () {
		return _ndSets.getNumber().intValue ();
	}

	// -------------- constructors for display
	
	void constructDataModels () {
		_ndSets = new SpinnerNumberModel (4, 2, 10, 1);
		super.constructDataModels ();
	}

	JPanel createParametersPanel () {
		return super.createParametersPanel ("ND Sets: ",
					"Number of non-dominated sets",
					new JSpinner (_ndSets));
	}
}
