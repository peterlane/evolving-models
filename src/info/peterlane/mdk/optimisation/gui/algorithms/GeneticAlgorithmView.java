package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.*;
import info.peterlane.mdk.optimisation.algorithms.GeneticAlgorithm;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

abstract public class GeneticAlgorithmView extends JInternalFrame {
	final Shell shell;
	private JPanel feedback;
	private JLabel feedbackLabel;
	private JButton createButton, evolveButton;
  protected final Experiment expt;

	protected SpinnerNumberModel 
		mutateProbability,	// likelihood of mutating a given individual
		populationSize,	// size of population to evolve
		numCycles;		// number of cycles of evolution

	public GeneticAlgorithmView (String title, Shell shell, 
			Experiment expt) {
		super (title, true, true, true, true);

		this.shell = shell;
    this.expt = expt;
		createFeedbackPanel ();

		add (feedback, BorderLayout.SOUTH);

		setMinimumSize (new Dimension (330, 300));
		setSize (330, 300);
	}

	// accessors to values
	int getMutateProbability () {
		return mutateProbability.getNumber().intValue ();
	}

	int getPopulationSize () {
		return populationSize.getNumber().intValue ();
	}

	int getNumberCycles () {
		return numCycles.getNumber().intValue ();
	}

	// -------------- actions
	private String freezeMessage = "Please wait ...";

	void setFreezeMessage (String msg) {
		freezeMessage = msg;
	}

	private final Runnable freezeGuiProcess = new Runnable () {
		public void run () {
			showFeedback (freezeMessage);
			hideControls ();
		}
	};

	void freezeGui () {
		SwingUtilities.invokeLater (freezeGuiProcess);
	}

	void unfreezeGui () {
		showControls ();
		hideFeedback ();
		SwingUtilities.invokeLater (updateObservers);
	}

	Runnable updateObservers;

	abstract void createPopulation ();
	abstract void evolvePopulation ();

	private void showFeedback (String msg) {
		feedback.setVisible (true);
		feedbackLabel.setText (msg);
	}

	private void hideFeedback () {
		feedback.setVisible (false);
	}

	private void hideControls () {
		createButton.setEnabled (false);
		evolveButton.setEnabled (false);
	}
	
	private void showControls () {
		createButton.setEnabled (true);
		evolveButton.setEnabled (true);
	}

	// -------------- construct the panels
	private void createFeedbackPanel () {
		feedback = new JPanel (new BorderLayout ());
		feedback.setBorder (new BevelBorder (BevelBorder.LOWERED));

		feedbackLabel = new JLabel ("", SwingConstants.LEFT);
		feedback.add (feedbackLabel, BorderLayout.WEST);
		feedback.setVisible (false);
	}

	void constructDataModels () {
		mutateProbability = new SpinnerNumberModel (10, 1, 100, 1);
		populationSize = new SpinnerNumberModel (100, 1, 10000, 1);
		numCycles = new SpinnerNumberModel (100, 1, 10000, 1);
	}

	JPanel createParametersPanel (String label, String tooltip, JComponent component) {
		JPanel panel = new JPanel (new BorderLayout ());
		panel.add (createControlsPanel (label, tooltip, component), BorderLayout.CENTER);

		return panel;
	}

	private JPanel createControlsPanel (String label, String tooltip, JComponent component) {
		JPanel panel = new JPanel (new GridLayout (1, 2));
		panel.add (Tools.makeTwoLabelledControls (label,
					tooltip,
					component,
					"Population: ",
					"Number of individuals in the population",
					new JSpinner (populationSize)));
		panel.add (Tools.makeTwoLabelledControls ("Mutate: ",
					"Likelihood of mutating a given individual (0-100)",
					new JSpinner (mutateProbability),
					"Cycles: ",
					"Number of evolutionary cycles to perform",
					new JSpinner (numCycles)));

		return panel;
	}

	JPanel createButtonPanel () {
		JPanel panel = new JPanel ();
		panel.setBorder (new EmptyBorder (5, 5, 5, 5));
		panel.setLayout (new BoxLayout (panel, BoxLayout.X_AXIS));

		createButton = new JButton ("Create");
		createButton.setToolTipText ("Create a population from given characteristics");
		createButton.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
				createPopulation ();
			}
		});
		panel.add (createButton);
		panel.add (Box.createHorizontalStrut (10));
		evolveButton = new JButton ("Evolve");
		evolveButton.setToolTipText ("Evolve the current population, using given parameters");
		evolveButton.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent e) {
				evolvePopulation ();
			}
		});
		panel.add (evolveButton);

		return panel;
	}
}
