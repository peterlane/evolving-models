package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.ConstraintDisplay;
import info.peterlane.mdk.optimisation.gui.ParameterDisplay;
import info.peterlane.mdk.optimisation.gui.ResultsTableView;
import info.peterlane.mdk.optimisation.gui.Shell;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.gui.models.ModelView;
import info.peterlane.mdk.optimisation.algorithms.MultipleTheoriesMultipleConstraintsND;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

public class MultipleTheoriesMultipleConstraintsNDView extends NonDominatedGeneticAlgorithmView {
	private static int numViews;
	private final MultipleTheoriesMultipleConstraintsND population;

	public MultipleTheoriesMultipleConstraintsNDView (Shell shell, 
			Experiment expt) {
		super ("Multiple Theories, Multiple Constraints [non-dominated]" + 
				( numViews > 0 ? " - " + numViews : ""), 
				shell, expt);
		numViews += 1;

		population = new MultipleTheoriesMultipleConstraintsND (expt);
		updateObservers = new Runnable () {
			public void run () { population.notifyObservers (); }
		};

		add (createGaControlPanel (), BorderLayout.NORTH);
		add (new MultiplePopulationView (shell, expt, population), BorderLayout.CENTER);

		setVisible (true);
	}

	// -------------- actions
	void createPopulation () {
		setFreezeMessage ("Please wait: creating populations ...");
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {

      @Override
			public Void doInBackground () {
				freezeGui ();
				population.create (getPopulationSize ());
        return null;
			}

      @Override
      protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	void evolvePopulation () {
		setFreezeMessage ("Please wait: evolving populations ...");
		SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer> () {

      @Override
			public Void doInBackground () {
				freezeGui ();
				for (int cycles = 0, n = getNumberCycles (); cycles < n; ++cycles) {
          try {
            population.evolve (
                getNumberSets (),
                getMutateProbability ());
            publish (cycles);
          } catch (IllegalArgumentException | java.lang.reflect.InvocationTargetException | NoSuchMethodException iee) {
            ; // ignore errors
          }
				}
        return null;
			}

      @Override
      protected void process (List<Integer> v) {
        updateObservers.run ();
      }

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	// -------------- construct the panels
	private JPanel createGaControlPanel () {
		constructDataModels ();

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (createParametersPanel ());
		panel.add (createButtonPanel ());
		panel.add (new JSeparator ());

		return panel;
	}

	class MultiplePopulationView extends JPanel implements ActionListener {
		private static final String 
			PARAMETERS_DISPLAY_GLOBAL = "Global Parameters",
			PARAMETERS_DISPLAY_LOCAL = "Local Parameters",
			RESULTS_TABLE = "Results",
			CONSTRAINTS_DISPLAY_LOCAL = "Local Constraints",
			CONSTRAINTS_DISPLAY_GLOBAL = "Global Constraints";
		private Shell shell;
		private Experiment expt;
		private MultipleTheoriesMultipleConstraintsND population;
		private JLabel title;
		private final JTabbedPane tabs;

		public MultiplePopulationView (Shell shell, 
				Experiment expt, 
				MultipleTheoriesMultipleConstraintsND population) {
			super ();
			this.shell = shell;
			this.expt = expt;
			this.population = population;

			setLayout (new BorderLayout ());

			JMenuBar showMenu = new JMenuBar ();
			showMenu.setBorder (new EmptyBorder (3, 3, 3, 3));
			showMenu.setAlignmentY (0.5f);

			JMenu show = new JMenu ("Show");
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Compute results against all constraints of non-dominated models",
						"Table of non-dominated model results", 
						RESULTS_TABLE));
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Show range of parameter values for globally non-dominated models in this theory",
						"Parameter values of globally non-dominated models",
						PARAMETERS_DISPLAY_GLOBAL));
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Show range of parameter values for locally non-dominated models in this theory",
						"Parameter values of locally non-dominated models",
						PARAMETERS_DISPLAY_LOCAL));
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Show range of constraint values for globally non-dominated models in this theory",
						"Constraint values of globally non-dominated models",
						CONSTRAINTS_DISPLAY_GLOBAL));
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Show range of constraint values for locally non-dominated models in this theory",
						"Constraint values of locally non-dominated models",
						CONSTRAINTS_DISPLAY_LOCAL));

			showMenu.add (show);

			title = new JLabel ("Current populations: ");
			Box titleBox = Box.createHorizontalBox ();
			titleBox.add (title);
			titleBox.add (Box.createHorizontalGlue ());
			titleBox.add (showMenu);
			add (titleBox, BorderLayout.NORTH);

			tabs = new JTabbedPane (JTabbedPane.BOTTOM);
			for (Class<? extends Model> theory : expt.getTheories ()) {
				tabs.addTab (Theory.classNameFor (theory),
						Tools.getIcon (theory),
						new JScrollPane (createTable (theory)));
			}
			add (tabs);
		}

		private Class<? extends Model> getTheory () {
			return expt.getTheories().get (tabs.getSelectedIndex ());
		}
		
		private JTable createTable (Class<? extends Model> theory) {
			ResultsView view = new ResultsView (theory);
			JTable table = new JTable (view);
			Tools.addStandardRenderers (table, view);
			table.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
			// make headers use a bold font
			table.getTableHeader().setFont (table.getTableHeader().getFont().deriveFont (Font.BOLD));
			// catch double clicks on the table, to show a selected model
			table.addMouseListener (new TableMouseListener (table, theory));
			// and on the header, to sort a column
			table.getTableHeader().addMouseListener (new HeaderMouseListener (
						table.getTableHeader (), theory));
			population.addObserver (view);

			return table;
		}

		public void actionPerformed (ActionEvent e) {
			String cmd = e.getActionCommand ();

			if (cmd == RESULTS_TABLE) {
				shell.addView (new ResultsTableView (population.toString (),
							population.getNonDominatedModels (),
							shell,
							expt));
			} else if (cmd == PARAMETERS_DISPLAY_GLOBAL) {
				shell.addView (new ParameterDisplay (getTheory ().toString () + " (Global) " + population.toString (),
							population.extractGlobalNonDominatedModels (getTheory ())));
			} else if (cmd == PARAMETERS_DISPLAY_LOCAL) {
				shell.addView (new ParameterDisplay (getTheory ().toString () + " (Local) " + population.toString (),
							population.extractLocalNonDominatedModels (getTheory ())));
			} else if (cmd == CONSTRAINTS_DISPLAY_GLOBAL) {
				shell.addView (new ConstraintDisplay (getTheory ().toString () + " (Global) " + population.toString (),
							population.extractGlobalNonDominatedModels (getTheory ()),
							expt));
			} else if (cmd == CONSTRAINTS_DISPLAY_LOCAL) {
				shell.addView (new ConstraintDisplay (getTheory ().toString () + " (Local) " + population.toString (),
							population.extractLocalNonDominatedModels (getTheory ()),
							expt));
			} 
		}

		class HeaderMouseListener implements MouseListener {
			private final JTableHeader header;
			private final Class<? extends Model> theory;

			HeaderMouseListener (JTableHeader header, Class<? extends Model> theory) {
				this.header = header;
				this.theory = theory;
			}

			public void mouseClicked (MouseEvent e) {
				if ((e.getClickCount() == 2) && (e.getButton() == MouseEvent.BUTTON1)) {
					final int column = header.columnAtPoint (e.getPoint ());
					if (column != 0) {
						setFreezeMessage ("Please wait: sorting column ...");
						SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
              @Override
							public Void doInBackground () {
								freezeGui ();
								population.sortByConstraint (
										theory,
										expt.getConstraints().get (column - 1));
                return null;
							}

              @Override
							protected void done () {
								population.notifyObservers ();
								unfreezeGui ();
							}
						};
						worker.execute ();
					}
				}
			}
			public void mouseEntered  (MouseEvent e) {}
			public void mouseExited   (MouseEvent e) {}
			public void mousePressed  (MouseEvent e) {}
			public void mouseReleased (MouseEvent e) {}
		}

		class TableMouseListener implements MouseListener {
			private final JTable table;
			private final Class<? extends Model> theory;

			TableMouseListener (JTable table, Class<? extends Model> theory) {
				this.table = table;
				this.theory = theory;
			}

			public void mouseClicked (MouseEvent e) {
				if ((e.getClickCount() == 2) && (e.getButton() == MouseEvent.BUTTON1)) {
					shell.addView (new ModelView (
                population.getPopulation(theory).get (
                  table.rowAtPoint(e.getPoint ())),
								expt
                )
              );
				}
			}
			public void mouseEntered  (MouseEvent e) {}
			public void mouseExited   (MouseEvent e) {}
			public void mousePressed  (MouseEvent e) {}
			public void mouseReleased (MouseEvent e) {}
		}

		class ResultsView extends DefaultTableModel implements Observer, Tools.TableCellHighlighter {
			private final Class<? extends Model> theory;

			public ResultsView (Class<? extends Model> theory) {
				super (population.getPopulation(theory).size (), 1 + expt.getConstraints().size ());
				this.theory = theory;
			}

			public Class getColumnClass (int c) {
				return (c == 0 ? String.class : double.class);
			}

			public int getColumnCount () {
				return 1 + expt.getConstraints().size ();
			}

			public String getColumnName (int c) {
				if (c == 0) { 
					return "Model";
				} else {
					return expt.getConstraints().get(c-1).toString ();
				}
			}

			public int getRowCount () {
				if (theory == null) { return 0; } // happens during creation
				return (population.getPopulation(theory).size ());
			}
			
			public boolean isCellEditable (int row, int col) { 
				return false;
			}

			public Object getValueAt (int r, int c) {
				if (population.getPopulation(theory).size () <= r) return "";
				Model model = population.getPopulation(theory).get (r);
				if (model == null) return ""; // sometimes happens in threading

				if (c == 0) {
					return model.toString ();
				} else { 
					return expt.getConstraints().get(c-1).computeMatch (expt.evaluate (model));
				}
			}

			// implement Observer interface -- ?? Only needs to be present once
			public void update (Observable o, Object arg) {
				title.setText ("Populations (after " + 
					population.getCycles () + 
					" cycle" + 
					(population.getCycles () == 1 ? "" : "s") +
					"): ");
				fireTableDataChanged ();
			}
			
			// implement TableCellHighlighter interface
			// -- highlight row if model is a non-dominated model
			public boolean isCellHighlighted (int r, int c) {
				if (population.getPopulation(theory).size () <= r) return false;
				Model model = population.getPopulation(theory).get (r);

				return population.isNonDominatedModel (model);
			}
		}
	}
}
