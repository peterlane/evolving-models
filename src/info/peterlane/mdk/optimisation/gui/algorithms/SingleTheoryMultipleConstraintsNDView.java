package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.ConstraintDisplay;
import info.peterlane.mdk.optimisation.gui.ParameterDisplay;
import info.peterlane.mdk.optimisation.gui.ResultsTableView;
import info.peterlane.mdk.optimisation.gui.Shell;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.gui.models.ModelView;
import info.peterlane.mdk.optimisation.algorithms.*;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

public class SingleTheoryMultipleConstraintsNDView extends NonDominatedGeneticAlgorithmView {
	private static int numViews;
	private final SingleTheoryMultipleConstraintsND population;
	private JComboBox<String> theorySelector;

	public SingleTheoryMultipleConstraintsNDView (Shell shell, 
			Experiment expt) {
		super ("Single Theory, Multiple Constraints [non-dominated]" + 
				( numViews > 0 ? " - " + numViews : ""), 
				shell, expt);
		numViews += 1;

		population = new SingleTheoryMultipleConstraintsND (expt);
		updateObservers = new Runnable () {
			public void run () { population.notifyObservers (); }
		};

		add (createGaControlPanel (), BorderLayout.NORTH);
		add (new PopulationView (shell, expt, population), BorderLayout.CENTER);

		setVisible (true);
	}

	// accessors to values
	private Class<? extends Model> getSelectedTheory () {
		return expt.getTheories().get (theorySelector.getSelectedIndex ());
	}

	// -------------- actions
	void createPopulation () {
		setFreezeMessage ("Please wait: creating population ...");
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				population.setTheory (getSelectedTheory ());
				population.create (getPopulationSize ());
        return null;
			}

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	void evolvePopulation () {
		setFreezeMessage ("Please wait: evolving population ...");
		SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				for (int cycles = 0, n = getNumberCycles (); cycles < n; ++cycles) {
					try {
					population.evolve (
						getNumberSets (),
						getMutateProbability ());
          publish (cycles);
					} catch (IllegalArgumentException | java.lang.reflect.InvocationTargetException | NoSuchMethodException iee) {
						; // ignore problems
					}
				}
        return null;
			}

      @Override
      protected void process (List<Integer> v) {
        updateObservers.run ();
      }

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	// -------------- construct the panels
	private JPanel createGaControlPanel () {
		theorySelector = Tools.createTheorySelector (expt);
		constructDataModels ();

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (Tools.makeLabelledControl (new JLabel ("Theory: ", SwingConstants.RIGHT),
					"Theory from which to select models",
					theorySelector));
		panel.add (createParametersPanel ());
		panel.add (createButtonPanel ());
		panel.add (new JSeparator ());

		return panel;
	}

	class PopulationView extends JPanel implements ActionListener {
		private static final String 
			name = "STMC [non-dominated]",
			PARAMETERS_DISPLAY = "Parameters",
			RESULTS_TABLE = "Results",
			CONSTRAINTS_DISPLAY = "Constraints";
		private Shell shell;
		private Experiment expt;
		private SingleTheoryMultipleConstraintsND population;
		private JLabel title;

		public PopulationView (Shell shell, 
				Experiment expt, 
				SingleTheoryMultipleConstraintsND population) {
			super ();
			this.shell = shell;
			this.expt = expt;
			this.population = population;

			setLayout (new BorderLayout ());

			JMenuBar showMenu = new JMenuBar ();
			showMenu.setBorder (new EmptyBorder (3, 3, 3, 3));
			showMenu.setAlignmentY (0.5f);

			JMenu show = new JMenu ("Show");
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Compute results against all constraints of non-dominated models",
						"Table of non-dominated model results", 
						RESULTS_TABLE));
			show.add (Tools.makeMenuItemWithFeedback (this, 
						shell.getFeedbackLabel (),
						"Show range of values taken by pairs of parameters",
						"Parameter values of non-dominated models", 
						PARAMETERS_DISPLAY));
			show.add (Tools.makeMenuItemWithFeedback (this,
						shell.getFeedbackLabel (),
						"Show range of constraint values for non-dominated models",
						"Constraint values of non-dominated models",
						CONSTRAINTS_DISPLAY));
			showMenu.add (show);

			title = new JLabel ("Current population: ");
			Box titleBox = Box.createHorizontalBox ();
			titleBox.add (title);
			titleBox.add (Box.createHorizontalGlue ());
			titleBox.add (showMenu);
			add (titleBox, BorderLayout.NORTH);

			ResultsView view = new ResultsView ();
			JTable fitnessView = new JTable (view);
			Tools.addStandardRenderers (fitnessView, view);
			fitnessView.setAutoResizeMode (JTable.AUTO_RESIZE_OFF);
			// make headers use a bold font
			fitnessView.getTableHeader().setFont (fitnessView.getTableHeader().getFont().deriveFont (Font.BOLD));
			// catch double clicks on the table, to show a selected model
			fitnessView.addMouseListener (new TableMouseListener (fitnessView));
			// and on the header, to sort a column
			fitnessView.getTableHeader().addMouseListener (new HeaderMouseListener (fitnessView.getTableHeader ()));

			add (new JScrollPane (fitnessView));
			population.addObserver (view);
		}

		public void actionPerformed (ActionEvent e) {
			String cmd = e.getActionCommand ();

			if (cmd == PARAMETERS_DISPLAY) {
				shell.addView (new ParameterDisplay (name + population.toString (),
							population.getNonDominatedModels ()));
			} else if (cmd == RESULTS_TABLE) {
				shell.addView (new ResultsTableView (name + population.toString (),
							population.getNonDominatedModels (),
							shell,
							expt));
			} else if (cmd == CONSTRAINTS_DISPLAY) {
				shell.addView (new ConstraintDisplay (name + population.toString (),
							population.getNonDominatedModels (),
							expt));
			}
		}

		class HeaderMouseListener implements MouseListener {
			private final JTableHeader header;

			HeaderMouseListener (JTableHeader header) {
				this.header = header;
			}

			public void mouseClicked (MouseEvent e) {
				if ((e.getClickCount() == 2) && (e.getButton() == MouseEvent.BUTTON1)) {
					final int column = header.columnAtPoint (e.getPoint ());
					if (column != 0) {
						setFreezeMessage ("Please wait: sorting column ...");
						SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
              @Override
							public Void doInBackground () {
								freezeGui ();
								population.sortByConstraint (expt.getConstraints().get (column - 1));
                return null;
							}

              @Override
							protected void done () {
								population.notifyObservers ();
								unfreezeGui ();
							}
						};
						worker.execute ();
					}
				}
			}
			public void mouseEntered  (MouseEvent e) {}
			public void mouseExited   (MouseEvent e) {}
			public void mousePressed  (MouseEvent e) {}
			public void mouseReleased (MouseEvent e) {}
		}

		class TableMouseListener implements MouseListener {
			private final JTable table;

			TableMouseListener (JTable table) {
				this.table = table;
			}

			public void mouseClicked (MouseEvent e) {
				if ((e.getClickCount() == 2) && (e.getButton() == MouseEvent.BUTTON1)) {
					shell.addView (new ModelView (
                population.getPopulation().get (
								table.rowAtPoint(e.getPoint ())),
								expt)
              );
				}
			}
			public void mouseEntered  (MouseEvent e) {}
			public void mouseExited   (MouseEvent e) {}
			public void mousePressed  (MouseEvent e) {}
			public void mouseReleased (MouseEvent e) {}
		}

		class ResultsView extends DefaultTableModel implements Observer, Tools.TableCellHighlighter {

			public ResultsView () {
				super (population.getPopulation().size (), 1 + expt.getConstraints().size ());
			}

			public Class getColumnClass (int c) {
				return (c == 0 ? String.class : double.class);
			}

			public int getColumnCount () {
				return 1 + expt.getConstraints().size ();
			}

			public String getColumnName (int c) {
				if (c == 0) { 
					return "Model";
				} else {
					return expt.getConstraints().get(c-1).toString ();
				}
			}

			public int getRowCount () {
				return (population.getPopulation().size ());
			}
			
			public boolean isCellEditable (int row, int col) { 
				return false;
			}

			public Object getValueAt (int r, int c) {
				if (population.getPopulation().size () <= r) return "";
				Model model = population.getPopulation().get (r);
				if (model == null) return ""; // sometimes happens in threading

				if (c == 0) {
					return model.toString ();
				} else { 
					return expt.getConstraints().get(c-1).computeMatch (expt.evaluate (model));
				}
			}

			// implement Observer interface
			public void update (Observable o, Object arg) {
				title.setText ("Current population (after " + 
					population.getCycles () + 
					" cycle" + 
					(population.getCycles () == 1 ? "" : "s") +
					"): ");
				fireTableDataChanged ();
			}
			
			// implement TableCellHighlighter interface
			// -- highlight row if model is a non-dominated model
			public boolean isCellHighlighted (int r, int c) {
				if (population.getPopulation().size () <= r) return false;
				Model model = population.getPopulation().get (r);

				return population.isNonDominatedModel (model);
			}

		}
	}
}
