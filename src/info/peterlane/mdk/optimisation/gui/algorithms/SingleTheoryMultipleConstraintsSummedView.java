package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.util.List;
import javax.swing.*;

import info.peterlane.mdk.optimisation.gui.Shell;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.algorithms.SingleCriterion;
import info.peterlane.mdk.optimisation.algorithms.SingleTheoryMultipleConstraintsSummed;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

public class SingleTheoryMultipleConstraintsSummedView extends EliteGeneticAlgorithmView {
	private static int numViews;
	private final SingleTheoryMultipleConstraintsSummed population;
	private JComboBox<String> theorySelector;

	public SingleTheoryMultipleConstraintsSummedView (Shell shell, Experiment expt) throws IllegalArgumentException {
		super ("Single Theory, Multiple Constraints [summed]" + ( numViews > 0 ? " - " + numViews : ""), 
				shell, expt);
    numViews += 1;

    population = new SingleTheoryMultipleConstraintsSummed (expt);
    updateObservers = new Runnable () {
      public void run () { population.notifyObservers (); }
    };

    add (createGaControlPanel (), BorderLayout.NORTH);
    add (new PopulationView (shell, expt, population), BorderLayout.CENTER);

		setVisible (true);
	}

	// accessors to values
	private Class<? extends Model> getSelectedTheory () {
		return expt.getTheories().get (theorySelector.getSelectedIndex ());
	}

	// -------------- actions
	void createPopulation () {
		setFreezeMessage ("Please wait: creating population ...");
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
        population.setTheory (getSelectedTheory ());
				population.create (getPopulationSize ());
        return null;
			}

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	void evolvePopulation () {
		setFreezeMessage ("Please wait: evolving population ...");
		SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				for (int cycles = 0, n = getNumberCycles (); cycles < n; ++cycles) {
          try {
            population.evolve (
                getEliteProportion (),
                getMutateProbability ());
            publish (cycles);
          } catch (IllegalArgumentException | java.lang.reflect.InvocationTargetException | NoSuchMethodException iee) {
            ;
          }
				}
        return null;
			}

      @Override
      protected void process (List<Integer> v) {
        updateObservers.run ();
      }

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	// -------------- construct the panels
	private JPanel createGaControlPanel () {
		theorySelector = Tools.createTheorySelector (expt);
		constructDataModels ();

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (Tools.makeLabelledControl (new JLabel ("Theory: ", SwingConstants.RIGHT),
					"Theory from which to select models",
					theorySelector));
		panel.add (createParametersPanel ());
		panel.add (createButtonPanel ());
		panel.add (new JSeparator ());

		return panel;
	}

	class PopulationView extends SinglePopulationTable {

		public PopulationView (Shell shell, Experiment expt, SingleCriterion population) {
			super ("STMC [summed]", shell, expt, population);
		}

		public Object computeModelValue (Model model) {
			return population.getSummedFitness (model);
		}

		public int retrieveEliteProportion () { return getEliteProportion (); }
	}
}
