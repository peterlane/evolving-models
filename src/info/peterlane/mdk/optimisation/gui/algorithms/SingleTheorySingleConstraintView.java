package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.*;
import info.peterlane.mdk.optimisation.algorithms.SingleCriterion;
import info.peterlane.mdk.optimisation.algorithms.SingleTheorySingleConstraint;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

public class SingleTheorySingleConstraintView extends EliteGeneticAlgorithmView {
	private static int numViews;
	private final SingleTheorySingleConstraint population;
	private JComboBox<String> theorySelector;
  private JComboBox<Constraint> constraintSelector;

	public SingleTheorySingleConstraintView (Shell shell, Experiment expt) throws IllegalArgumentException {
		super ("Single Theory, Single Constraint" + ( numViews > 0 ? " - " + numViews : ""), 
				shell, expt);
		numViews += 1;

    population = new SingleTheorySingleConstraint (expt);
    updateObservers = new Runnable () {
      public void run () { population.notifyObservers (); }
    };

    add (createGaControlPanel (), BorderLayout.NORTH);
    add (new PopulationView (shell, expt, population), BorderLayout.CENTER);

    setVisible (true);
	}

	// -- accessors to values
	private Class<? extends Model> getSelectedTheory () {
		return expt.getTheories().get (theorySelector.getSelectedIndex ());
	}

	private Constraint getSelectedConstraint () {
		return (Constraint)constraintSelector.getSelectedItem ();
	}

	// -------------- actions
	void createPopulation () {
		setFreezeMessage ("Please wait: creating population ...");
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
        population.setConstraint (getSelectedConstraint ());
        population.setTheory (getSelectedTheory ());
				population.create (getPopulationSize ());
        return null;
			}

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	void evolvePopulation () {
		setFreezeMessage ("Please wait: evolving population ...");
		SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				for (int cycles = 0, n = getNumberCycles (); cycles < n; ++cycles) {
          try {
					population.evolve (
						getEliteProportion (),
						getMutateProbability ());
          } catch (IllegalArgumentException | java.lang.reflect.InvocationTargetException | NoSuchMethodException iee) {
            assert (false); // program error 
          }
          publish (cycles);
				}
        return null;
			}

      @Override
      protected void process (List<Integer> v) {
        updateObservers.run ();
      }

      @Override
      protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	// -------------- construct the panels
	private JPanel createGaControlPanel () {
		theorySelector = Tools.createTheorySelector (expt);
		constraintSelector = new JComboBox<Constraint> (expt.getConstraints().toArray(new Constraint[expt.getConstraints().size()]));
		constructDataModels ();

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (Tools.makeTwoLabelledControls ("Theory: ",
					"Theory from which to select models",
					theorySelector,
					"Constraint: ",
					"Constraint to use for fitness measure",
					constraintSelector));
		panel.add (createParametersPanel ());
		panel.add (createButtonPanel ());
		panel.add (new JSeparator ());

		return panel;
	}

	class PopulationView extends SinglePopulationTable {
		private Experiment expt;
		
		public PopulationView (Shell shell, Experiment expt, SingleCriterion population) {
			super ("STSC", shell, expt, population);

			this.expt = expt;
		}

		public Object computeModelValue (Model model) {
			return getSelectedConstraint().computeMatch (expt.evaluate (model));
		}

		public int retrieveEliteProportion () { return getEliteProportion (); }
	}
}
