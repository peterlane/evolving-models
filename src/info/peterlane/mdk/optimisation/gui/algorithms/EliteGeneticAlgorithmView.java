package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.*;
import info.peterlane.mdk.optimisation.algorithms.*;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

abstract public class EliteGeneticAlgorithmView extends GeneticAlgorithmView {
	private SpinnerNumberModel 
		eliteProportion;	// proportion of population to preserve and create from

	public EliteGeneticAlgorithmView (String title, Shell shell, 
			Experiment expt) {
		super (title, shell, expt);
	}

	// accessors to values
	int getEliteProportion () {
		return eliteProportion.getNumber().intValue ();
	}

	// -------------- constructors for display
	
	void constructDataModels () {
		eliteProportion = new SpinnerNumberModel (10, 1, 100, 1);
		super.constructDataModels ();
	}

	JPanel createParametersPanel () {
		return super.createParametersPanel ("Elite: ",
					"Number of population to keep each cycle",
					new JSpinner (eliteProportion));
	}
}
