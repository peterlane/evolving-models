package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.util.List;
import javax.swing.*;

import info.peterlane.mdk.optimisation.gui.Shell;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.algorithms.MultipleTheoriesSingleConstraint;
import info.peterlane.mdk.optimisation.algorithms.SingleCriterion;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

public class MultipleTheoriesSingleConstraintView extends EliteGeneticAlgorithmView {
	private static int numViews;
	private final MultipleTheoriesSingleConstraint population;
	private JComboBox<Constraint> constraintSelector;

	public MultipleTheoriesSingleConstraintView (Shell shell, Experiment expt) throws IllegalArgumentException {
		super ("Multiple Theories, Single Constraint" + 
				(numViews > 0 ? " - " + numViews : ""),
				shell, expt);
		numViews += 1;

		population = new MultipleTheoriesSingleConstraint (expt);
		updateObservers = new Runnable () {
			public void run () { population.notifyObservers (); }
		};

		add (createGaControlPanel (), BorderLayout.NORTH);
		add (new MultiplePopulationView (shell, expt, population), BorderLayout.CENTER);

		setVisible (true);
	}

	// accessors to values
	private Constraint getSelectedConstraint () {
		return (Constraint)constraintSelector.getSelectedItem ();
	}

	// -------------- actions
	void createPopulation () {
		setFreezeMessage ("Please wait: creating populations ...");
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				population.create (getPopulationSize ());
        for (Class<? extends Model> theory : expt.getTheories ()) {
          population.getPopulation(theory).setConstraint (getSelectedConstraint ());
        }
        return null;
			}

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	void evolvePopulation () {
		setFreezeMessage ("Please wait: evolving populations ...");
		SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				for (int cycles = 0, n = getNumberCycles (); cycles < n; ++cycles) {
          try {
            population.evolve (
                getEliteProportion (),
                getMutateProbability ());
            publish (cycles);
          } catch (IllegalArgumentException | java.lang.reflect.InvocationTargetException | NoSuchMethodException iee) {
            ; // ignore error
          }
				}
        return null;
			}

      @Override
      protected void process (List<Integer> v) {
        updateObservers.run ();
      }

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	// -------------- construct the panels
	private JPanel createGaControlPanel () {
    constraintSelector = new JComboBox<Constraint> (expt.getConstraints().toArray(new Constraint[expt.getConstraints().size()]));
		constructDataModels ();

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (Tools.makeLabelledControl (new JLabel ("Constraint: ", SwingConstants.RIGHT),
					"Constraint to use for fitness measure",
					constraintSelector));
		panel.add (createParametersPanel ());
		panel.add (createButtonPanel ());
		panel.add (new JSeparator ());

		return panel;
	}

	class NewSinglePopulationTable extends SinglePopulationTable {
		private Experiment expt;

		public NewSinglePopulationTable (String name, 
				Shell shell, Experiment expt,
				SingleCriterion population) {
			super (name, shell, expt, population);
			
			this.expt = expt;
		}

		public int retrieveEliteProportion () { return getEliteProportion (); }
		public Object computeModelValue (Model model) { 
			return getSelectedConstraint().computeMatch (expt.evaluate (model)); 
		}
	}
	
	class MultiplePopulationView extends JTabbedPane {

		public MultiplePopulationView (Shell shell, Experiment expt,
				MultipleTheoriesSingleConstraint population) {
			super (JTabbedPane.BOTTOM);

			for (Class<? extends Model> theory : expt.getTheories ()) {
				addTab (Theory.classNameFor (theory),
						Tools.getIcon (theory),
						new NewSinglePopulationTable ("MTSC [separate]", 
							shell, expt, 
							population.getPopulation (theory)));
			}
		}
	}
}

