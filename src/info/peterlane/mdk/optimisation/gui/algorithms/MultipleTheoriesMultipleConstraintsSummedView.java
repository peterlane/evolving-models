package info.peterlane.mdk.optimisation.gui.algorithms;

import java.awt.*;
import java.util.List;
import javax.swing.*;

import info.peterlane.mdk.optimisation.algorithms.MultipleTheoriesMultipleConstraintsSummed;
import info.peterlane.mdk.optimisation.algorithms.SingleTheoryMultipleConstraintsSummed;
import info.peterlane.mdk.optimisation.gui.Shell;
import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.Constraint;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

public class MultipleTheoriesMultipleConstraintsSummedView extends EliteGeneticAlgorithmView {
	private static int numViews;
	private final MultipleTheoriesMultipleConstraintsSummed population;

	public MultipleTheoriesMultipleConstraintsSummedView (Shell shell, 
			Experiment expt ) throws IllegalArgumentException {
		super ("Multiple Theories, Multiple Constraints [summed]" + 
				(numViews > 0 ? " - " + numViews : ""),
				shell, expt);
		numViews += 1;

		population = new MultipleTheoriesMultipleConstraintsSummed (expt);
		updateObservers = new Runnable () {
			public void run () { population.notifyObservers (); }
		};

		add (createGaControlPanel (), BorderLayout.NORTH);
		add (new MultiplePopulationView (shell, expt, population), BorderLayout.CENTER);

		setVisible (true);
	}

	// -------------- actions
	void createPopulation () {
		setFreezeMessage ("Please wait: creating populations ...");
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				population.create (getPopulationSize ());
        return null;
			}

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	void evolvePopulation () {
		setFreezeMessage ("Please wait: evolving populations ...");
		SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer> () {
      @Override
			public Void doInBackground () {
				freezeGui ();
				for (int cycles = 0, n = getNumberCycles (); cycles < n; ++cycles) {
          try {
            population.evolve (
                getEliteProportion (),
                getMutateProbability ());
            publish (cycles);
          } catch (IllegalArgumentException | java.lang.reflect.InvocationTargetException | NoSuchMethodException iee) {
            ;
          }
				}
        return null;
			}

      @Override
      protected void process (List<Integer> v) {
        updateObservers.run ();
      }

      @Override
			protected void done () {
				unfreezeGui ();
			}
		};
		worker.execute ();
	}

	// -------------- construct the panels
	private JPanel createGaControlPanel () {
		constructDataModels ();

		JPanel panel = new JPanel ();
		panel.setLayout (new BoxLayout (panel, BoxLayout.Y_AXIS));
		panel.add (createParametersPanel ());
		panel.add (createButtonPanel ());
		panel.add (new JSeparator ());

		return panel;
	}

	class NewSinglePopulationTable extends SinglePopulationTable {
		private Experiment expt;
		private SingleTheoryMultipleConstraintsSummed singlepopulation;

		public NewSinglePopulationTable (String name, 
				Shell shell, Experiment expt, 
				SingleTheoryMultipleConstraintsSummed population) {
			super (name, shell, expt,  population);
			
			this.expt = expt;
			singlepopulation = population;
		}

		public int retrieveEliteProportion () { return getEliteProportion (); }
		public Object computeModelValue (Model model) { 
			return singlepopulation.getSummedFitness (model);
		}
	}
	
	class MultiplePopulationView extends JTabbedPane {

		public MultiplePopulationView (Shell shell, Experiment expt, 
				MultipleTheoriesMultipleConstraintsSummed population) {
			super (JTabbedPane.BOTTOM);

			for (Class<? extends Model> theory : expt.getTheories ()) {
				addTab (Theory.classNameFor (theory),
						Tools.getIcon (theory),
						new NewSinglePopulationTable ("MTMC [separate+summed]", 
							shell, expt, 
							population.getPopulation (theory)));
			}
		}
	}
}

