package info.peterlane.mdk.optimisation.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;
import info.peterlane.mdk.optimisation.gui.algorithms.SingleTheorySingleConstraintView;
import info.peterlane.mdk.optimisation.gui.algorithms.SingleTheoryMultipleConstraintsSummedView;
import info.peterlane.mdk.optimisation.gui.algorithms.SingleTheoryMultipleConstraintsNDView;
import info.peterlane.mdk.optimisation.gui.algorithms.MultipleTheoriesSingleConstraintView;
import info.peterlane.mdk.optimisation.gui.algorithms.MultipleTheoriesMultipleConstraintsSummedView;
import info.peterlane.mdk.optimisation.gui.algorithms.MultipleTheoriesMultipleConstraintsNDView;
import info.peterlane.mdk.optimisation.gui.models.ModelView;

/** Shell program to manage algorithms for evolving cognitive models.
 * Shell uses a desktop environment to contain different views.
 * Models may be explored individually or evolved using a variety of 
 * techniques.
 * @author Peter Lane
 */
public class Shell extends JFrame implements ActionListener {
	private final JDesktopPane desktop; // to hold the internal views
	private static JLabel feedback;     // provide a help message explaining menu options
	private static final String          // names for menu command strings
		ABOUT = "About",
		EXIT = "Exit",
		VIEW_DATA = "View Data",
		STSC = "STSC",
		STMC_SUMMED = "STMC SUMMED",
		STMC_ND = "STMC ND",
		MTSC = "MTSC",
		MTMC_SUMMED = "MTMC SUMMED",
		MTMC_ND = "MTMC ND",
		SHOW_DATA_HELP = "Show Data Help",
		SHOW_EXPLORE_HELP = "Show Explore Help",
		SHOW_DEVELOP_HELP = "Show Develop Help";

	private final Experiment expt; // pointer to current experiment setup
  private final JInternalFrame dataview; // optional frame for displaying data

  public Shell (Experiment expt) {
    this (expt, null);
  }

	public Shell (Experiment expt, JInternalFrame dataview) {
		super ("Evolving Cognitive Models");

		this.expt = expt;
    this.dataview = dataview;

		setSize (550, 500);
		
		desktop = new JDesktopPane ();
		desktop.setBackground (new Color (200, 200, 255));
		add (desktop);

    feedback = new JLabel ("Welcome to 'Evolving Cognitive Models' shell");
		feedback.setBorder (new BevelBorder (BevelBorder.LOWERED));
		add (feedback, BorderLayout.SOUTH);
		
		setJMenuBar (createMenuBar ()); // NB: Create menu after feedback

		// catch close-window event
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) { 
				exitFrame (); 
			}
		});

		setVisible (true);
    setTheme ("Metal");
	}

	/** Identify and perform the actions requested from interface */
	public void actionPerformed (ActionEvent e) {
    try {
      switch (e.getActionCommand ()) {
        case EXIT:
          exitFrame (); 
          break;
        case ABOUT:
          JOptionPane.showMessageDialog(this, 
              "<HTML><H2>Shell for Evolving Cognitive Models</H2>" + 
              "<P>Options under <i>Explore</i> let you view or<BR>interact with the data and model types.</P>" +
              "<P>Options under <i>Develop</i> provide an interface to<BR>the algorithms which create cognitive models.</P>" +
              "<P><HR><P>Copyright (c) 2007-13, Peter Lane.</P></HTML>",
              "About Program", 
              JOptionPane.INFORMATION_MESSAGE);
          break;
        case VIEW_DATA:
          if (dataview != null) {
            addView (dataview);
          }
          break;
        case STSC:
          addView (new SingleTheorySingleConstraintView (this, expt));
          break;
        case STMC_SUMMED:
          addView (new SingleTheoryMultipleConstraintsSummedView (this, expt));
          break;
        case STMC_ND:
          addView (new SingleTheoryMultipleConstraintsNDView (this, expt));
          break;
        case MTSC:
          addView (new MultipleTheoriesSingleConstraintView (this, expt));
          break;
        case MTMC_SUMMED:
          addView (new MultipleTheoriesMultipleConstraintsSummedView (this, expt));
          break;
        case MTMC_ND:
          addView (new MultipleTheoriesMultipleConstraintsNDView (this, expt));
          break;
        case SHOW_DATA_HELP:
          showHelp (HelpDialog.DATA_HELP);
          break;
        case SHOW_EXPLORE_HELP:
          showHelp (HelpDialog.EXPLORE_HELP);
          break;
        case SHOW_DEVELOP_HELP:
          showHelp (HelpDialog.DEVELOP_HELP);
          break;
      }
    } catch (IllegalArgumentException iee) {
      ; // Program error
    }
	}

	/** Tidy up and exit */
	private void exitFrame () {
		setVisible (false);
		dispose ();
		System.exit (0);
	}

	/** Manage help dialog */
	private HelpDialog helpDialog = null;
	private void showHelp (int helpType) {
		if (helpDialog == null) {
			helpDialog = new HelpDialog ();
			addView (helpDialog);
		} else if (!helpDialog.isVisible ()) {
			helpDialog.setVisible (true);
			addView (helpDialog);
		} else {
			Tools.selectInternalFrame (helpDialog);
		}
		helpDialog.showHelp (helpType);
	}

	/** Retrieve the label used for menu feedback */
	public JLabel getFeedbackLabel () { return feedback; }

	/** Used to place a new view in a new position, arranging views in a vertical stagger */
	private int lastX, lastY;
	public void addView (JInternalFrame view) {
		// place the view
		view.setLocation (lastX, lastY);
		// update the next view position, using this window as a guide
		lastY += 30;
		if (lastY + view.getHeight () > desktop.getHeight ()) {
			lastY = 0;
			lastX += 30;
			if (lastX + view.getWidth () > desktop.getWidth ()) {
				lastX = 0;
			}
		}
		// add the view
		desktop.add (view);
		try { view.setSelected (true); }
		catch (java.beans.PropertyVetoException pve) {}
	}

	private JMenuBar createMenuBar () {
		JMenuBar menu = new JMenuBar ();

		// -- program menu
		JMenu main = new JMenu ("Program");
		main.setMnemonic ('P');
		main.add (Tools.makeMenuItemWithFeedback (this, feedback, "About this program",
					"About", "About24.gif", 'A', ABOUT));
		main.addSeparator ();
		main.add (Tools.makeMenuItemWithFeedback (this, feedback, "Exit this program",
					"Exit", 'X', EXIT));
		menu.add (main);

		// -- explore menu
		JMenu explore = new JMenu ("Explore");
		explore.setMnemonic ('E');
    if (dataview != null) {
      explore.add (Tools.makeMenuItemWithFeedback (this, feedback, 
            "Show instances and target behaviour",
            "Data", 'D', VIEW_DATA));
      explore.addSeparator ();
    }
    for (Class<? extends Model> theory : expt.getTheories ()) {
      explore.add (new ModelViewAction (this, theory, expt));
    }
		menu.add (explore);

		// -- develop menu
		JMenu develop = new JMenu ("Develop");
		develop.setMnemonic ('D');
		develop.add (Tools.makeMenuItemWithFeedback (this,
					feedback, "Single Theory, Single Constraint",
					"STSC", STSC));
		develop.addSeparator ();
		develop.add (Tools.makeMenuItemWithFeedback (this,
					feedback, "Single Theory, Multiple Constraints - fitness is sum of constraints",
					"STMC - summed", STMC_SUMMED));
		develop.add (Tools.makeMenuItemWithFeedback (this,
					feedback, "Single Theory, Multiple Constraints - non-dominated sorting",
					"STMC - non-dominated", STMC_ND));
		develop.addSeparator ();
		develop.add (Tools.makeMenuItemWithFeedback (this,
					feedback, "Multiple Theories, Single Constraint",
					"MTSC", MTSC));
		develop.addSeparator ();
		develop.add (Tools.makeMenuItemWithFeedback (this,
					feedback, "Multiple Constraints and Theories - fitness is sum of constraints",
					"MTMC - summed", MTMC_SUMMED));
		develop.add (Tools.makeMenuItemWithFeedback (this,
					feedback, "Multiple Constraints and Theories - non-dominated sorting",
					"MTMC - non-dominated", MTMC_ND));
		menu.add (develop);

		// -- help menu
		JMenu help = new JMenu ("Help");
		help.setMnemonic ('H');
		help.add (Tools.makeMenuItemWithFeedback (this, 
					feedback, "Help on the view of the behavioural data",
					"Explore Data",   'T', SHOW_DATA_HELP));
		help.add (Tools.makeMenuItemWithFeedback (this, 
					feedback, "Help on the dialogs to explore individual theories",
					"Explore Models", 'E', SHOW_EXPLORE_HELP));
		help.add (Tools.makeMenuItemWithFeedback (this, 
					feedback, "Help on the learning algorithms to develop new models",
					"Develop Models", 'D', SHOW_DEVELOP_HELP));
		menu.add (help);

		return menu;
	}

  private class ModelViewAction extends AbstractAction implements ActionListener {
    private final Shell parent;
    private final Class<? extends Model> theory;
    private final Experiment expt;

    ModelViewAction (Shell parent, Class<? extends Model> theory, Experiment expt) {
      super (Theory.classNameFor (theory), Tools.getIcon (theory));

      this.parent = parent;
      this.theory = theory;
      this.expt = expt;
    }

    public void actionPerformed (ActionEvent e) {
      parent.addView (new ModelView (theory, expt));
    }
  }

	private JMenuItem disableMenu (JMenuItem mi) {
		mi.setEnabled (false);
		return mi;
	}

  private void setTheme (String theme) {
    try { 
      for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if (theme.equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (UnsupportedLookAndFeelException e) {
    } catch (ClassNotFoundException e) {
    } catch (InstantiationException e) {
    } catch (IllegalAccessException e) {
    }
    // make sure all components are updated
    SwingUtilities.updateComponentTreeUI(this);
    if (dataview != null) {
      SwingUtilities.updateComponentTreeUI (dataview);
    }
  }

}

class HelpDialog extends JInternalFrame {
	public static final int 
		DATA_HELP = 0,
		EXPLORE_HELP = 1,
		DEVELOP_HELP = 2;
	private static final String[]
		help = { "<html><body><h1>Data Display</h1>" +
			"<p>An optional display provided by the calling code.  Should list the " +
      "constraints used in testing, and possibly the instances as well.</p>" +
			"</body></html>",
			
			"<html><body><h1>Explore Models</h1>" +
			"Theories are provided by the exeriment used to instantiate the shell.  You can 'explore' the " +
			"performance of members of each theory by selecting the appropriate 'Explore' menu option." +
			"Selecting the menu option will open a dialog, displaying a model and its results." +
			"<p>On the left, there is space to enter values for each of the model's attributes." +
			"The 'Random' button will fill the attributes with random values.  " + "The 'Apply' " +
			"button is used to see the performance of the model." +
			"The first table on the right will show the performance of the model on each instance, " +
      "if this is supported by the calling experiment." +
			"The second (or only) table on the right will show the performance of the model " +
      "measured against each of the included constraints." +
			"</body></html>",
			
			"<html><body><h1>Develop Models</h1>" +
			"Models are developed using evolutionary techniques.  Several algorithms are " +
			"provided, and their performance may be compared.  Each algorithm may be " +
			"characterised based on the number of theories and constraints it considers.  " +
			"Where more than one theory or constraint are considered, further options are possible. "+
			"<p>For <em>multiple theories</em>, all theories are evolved in <em>separate</em> populations</p>" +
			"<p>For <em>multiple constraints</em>: " +
			"<ul><li> summed - where values for all constraints are added to make the fitness value</li>" +
			"<li> non-dominated - where the non-dominated models are selected and preferentially used for creating new instances</li>" +
			"</ul></p>" +
			"<h2>Single theory, single constraint</h2>" +
			"<p>" +
			"Used to optimise models from a single theory against one of the available constraints.  As each constraint produces a numeric measure of fitness, this measure is used to guide the evolutionary process." +
			"</p>" +
			"<h2>Single theory, multiple constraints - summed</h2>" +
			"<p>" +
			"Used to optimise models from a single theory against all the available constraints.  The fitness measure from all of the constraints is added up to produce a single measure of fitness." + 
			"</p>" +
			"<h2>Single theory, multiple constraints - non-dominated</h2>" +
			"<p>" +
			"Used to optimise models from a single theory against all the available constraints.  Models are selected for cross-over based on whether they are non-dominated: non-dominated models are those which are not worse than any other model." +
			"</p>" +
			"<h2>Multiple theories, single constraint</h2>" +
			"<p>" +
			"The same as with single-theory-single-constraint, except that every theory is evolved at the same time." +
			"</p>" +
			"<h2>Multiple theories, multiple constraints - summed</h2>" +
			"<p>" +
			"The same as with single-theory-multiple-constraint-summed, except that every theory is evolved at the same time." +
			"</p>" +
			"<h2>Multiple theories, multiple constraints - non-dominated</h2>" +
			"<p>Also known as <em>Speciated Non-Dominated Sorting Genetic Algorithm</em> (Lane and Gobet, 2005)</p>" +
			"<p>" +
			"Used to optimise models from multiple theories against all the available constraints.  Models are selected for cross-over based on whether they are non-dominated: non-dominated models are those which are not worse than any other model for <em>all</em> competing theories." +
			"</p>" +
			"</body></html>"
		};
	private JEditorPane helpPane;
	
	public HelpDialog () {
		super ("Help -- Describes Options", true, true, true, true);

		helpPane = new JEditorPane ();
		helpPane.setContentType ("text/html");
		helpPane.setEditable (false);
		add (new JScrollPane (helpPane));
		
		addInternalFrameListener (new InternalFrameAdapter () {
			public void internalFrameClosing (InternalFrameEvent ev) { 
				setVisible (false);
			}
		});
		
		setFrameIcon (Tools.makeIcon ("Help16.gif"));
		setSize (300, 200);
		setVisible (true);
	}

	public void showHelp (int helpType) {
		helpPane.setText (help[helpType]);
		helpPane.setCaretPosition (0);
	}

}

