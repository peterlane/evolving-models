package info.peterlane.mdk.optimisation.gui.models;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.text.*;

import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;
import info.peterlane.mdk.optimisation.Theory;

public class ModelView extends JInternalFrame {
	static final boolean WITH_BUTTONS = true, WITHOUT_BUTTONS = false;
	ResultsView resultsView;
  private Model model;

	public ModelView (Class<? extends Model> theory, Experiment expt) {
		super ("Model View: " + Theory.classNameFor (theory), true, true, true, true);

		resultsView = new ResultsView (expt);
    model = null;
    try {
      model = theory.getDeclaredConstructor().newInstance ();
    } catch (IllegalAccessException | InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException ie) {
      ; // ignore error - program fault
    }

    setFrameIcon (Tools.getIcon (theory));

		add (createParametersView (true), BorderLayout.WEST);
		add (resultsView, BorderLayout.CENTER);

		setSize (500, 300);
		setVisible (true);
	}

  public ModelView (Model model, Experiment expt) {
    super ("Model: " + model.toString (), true, true, true, true);

		resultsView = new ResultsView (expt);
    this.model = model;

    setFrameIcon (Tools.getIcon (model.getClass ()));

		add (createParametersView (false), BorderLayout.WEST);
		add (resultsView, BorderLayout.CENTER);
    resultsView.update (model);

		setSize (500, 300);
		setVisible (true);
  }

	private Box createParametersView (boolean showButtons) {
		Box panel = Box.createVerticalBox ();
		panel.setBorder (new EmptyBorder (3, 3, 3, 3));

		if (showButtons) {
			panel.add (createInputView ());
			Box buttons = Box.createHorizontalBox ();
			buttons.setBorder (new EmptyBorder (3, 3, 3, 3));
			JButton random = new JButton ("Random");
			random.setToolTipText ("Set some random values and then apply");
			random.addActionListener (new ActionListener () {
				public void actionPerformed (ActionEvent e) {
					fillFieldsRandomly ();
					applyModel ();
				}
			});
			buttons.add (random);
			buttons.add (Box.createHorizontalStrut (10));
			JButton apply = new JButton ("Apply");
			apply.setToolTipText ("Normalise values and then apply");
			apply.addActionListener (new ActionListener () {
				public void actionPerformed (ActionEvent e) {
					applyModel ();
				}
			});
			buttons.add (apply);
			panel.add (buttons);
		} else {
			panel.add (createDisplayView ());
		}

		return panel;
	}

  private JLabel[] parameterLabels;

	JPanel createDisplayView () {
    parameterLabels = new JLabel[model.getParameterCount ()];

    JPanel panel = new JPanel (new GridLayout (model.getParameterCount (), 2, 10, 5));
    for (int i = 0; i < model.getParameterCount (); i += 1) {
      parameterLabels[i] = Tools.makeValueLabel ("");
      panel.add (new JLabel ("" + model.getParameterName (i) + ":", SwingConstants.RIGHT));
      panel.add (parameterLabels[i]);
      Tools.changeLabel (parameterLabels[i], model.getParameter (i));
    }

    return panel;
  }

  private void setDisplayValues () {
    for (int i = 0; i < model.getParameterCount (); i += 1) {
      Tools.changeLabel (parameterLabels[i], model.getParameter (i));
    }
    resultsView.update (model);
  }

  private JFormattedTextField[] parameterInputs;

	JPanel createInputView () {
    NumberFormat fmt = NumberFormat.getNumberInstance ();
    fmt.setMinimumFractionDigits (3);

    parameterInputs = new JFormattedTextField[model.getParameterCount ()];

    JPanel panel = new JPanel (new GridLayout (model.getParameterCount (), 2, 10, 5));
    for (int i = 0; i < model.getParameterCount (); i += 1) {
      parameterInputs[i] = new JFormattedTextField (fmt);
      parameterInputs[i].setValue (Double.valueOf (0.0));
      panel.add (new JLabel ("" + model.getParameterName (i) + ":", SwingConstants.RIGHT));
      panel.add (parameterInputs[i]);
    }

    panel.setMaximumSize (panel.getPreferredSize ());

    return panel;
  }

  private void updateInputs () {
    for (int i = 0; i < model.getParameterCount (); i += 1) {
      parameterInputs[i].setValue (model.getParameter (i));
    }
  }

	void fillFieldsRandomly () {
    try {
      Model newModel = model.getClass().getDeclaredConstructor().newInstance ();
      for (int i = 0; i < model.getParameterCount (); i += 1) {
        model.setParameter (i, newModel.getParameter (i));
      }
    } catch (IllegalAccessException | InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException ie) {
      ; // ignore error
    }
    updateInputs ();
    resultsView.update (model);
  }

	void applyModel () {
    try {
      Model newModel = model.getClass().getDeclaredConstructor().newInstance ();
      for (int i = 0; i < model.getParameterCount (); i += 1) {
        newModel.setParameter (i, Tools.getFieldValue (parameterInputs[i]));
      }
      model = newModel;
    } catch (IllegalAccessException | InstantiationException | java.lang.reflect.InvocationTargetException | NoSuchMethodException ie) {
      ; // ignore error
    }
    updateInputs ();
    resultsView.update (model);
  }
}

