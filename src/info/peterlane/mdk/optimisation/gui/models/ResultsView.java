package info.peterlane.mdk.optimisation.gui.models;

import java.awt.*;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

import info.peterlane.mdk.optimisation.gui.Tools;
import info.peterlane.mdk.optimisation.*;
import info.peterlane.mdk.optimisation.Experiment;
import info.peterlane.mdk.optimisation.Model;

public class ResultsView extends JPanel {
	private final Experiment expt;
	private InstanceResultsModel instanceResultsModel = null;
	private ConstraintsResultsModel constraintsResultsModel;

	public ResultsView (Experiment expt) {
		super ();

		this.expt = expt;

    if (InstanceResultsModel.numInstances (expt) == 0) {
      setLayout (new GridLayout (1, 1));
    } else {
  		setLayout (new GridLayout (1, 2));
	  	add (createInstanceResultsTable ());
    }
		add (createConstraintsResultsTable ());
	}

	public void update (Model model) {
    if (instanceResultsModel != null) {
		  instanceResultsModel.setModel (model);
    }
		constraintsResultsModel.setModel (model);
	}

	private JPanel createInstanceResultsTable () {
		instanceResultsModel = new InstanceResultsModel (expt);
		return createResultsTable ("Instance results:", instanceResultsModel);
	}

	private JPanel createConstraintsResultsTable () {
		constraintsResultsModel = new ConstraintsResultsModel (expt);
		return createResultsTable ("Constraint results:", constraintsResultsModel);
	}

	private JPanel createResultsTable (String title, DefaultTableModel tableModel) {
		JTable table = new JTable (tableModel);
		Tools.addStandardRenderers (table);
		// make headers use a bold font
		table.getTableHeader().setFont (table.getTableHeader().getFont().deriveFont (Font.BOLD));

		JPanel panel = new JPanel (new BorderLayout ());
		JLabel name = new JLabel (title);
		name.setBorder (new EmptyBorder (3, 3, 3, 3));
		panel.add (name, BorderLayout.NORTH);
		panel.add (new JScrollPane (table), BorderLayout.CENTER);

		return panel;
	}
}

class InstanceResultsModel extends DefaultTableModel {
	private Model model;
	private List<Double> results;
	private final Experiment expt;
	private final String[] headers = new String[] { "Instance", "Prob (A)" };
	
	InstanceResultsModel (Experiment expt) {
		super (numInstances (expt), 2);
		this.expt = expt;
	}

	public void setModel (Model model) {
		this.model = model;
		results = expt.evaluate (model);
		fireTableRowsUpdated (0, testInstances.size () - 1);
	}

	public String getColumnName (int c) {
		return headers[c];
	}

	public Class getColumnClass (int c) {
		return (c == 0 ? String.class : double.class);
	}
	
	public Object getValueAt (int r, int c) {
		if (results == null || results.size () <= r) return "";

		if (c == 0) {
			return testInstances.get(r).toString();
		} else { // (c == 1)
			return results.get (r);
		}
	}

	public boolean isCellEditable (int row, int col) {
		return false;
	}

	private static List testInstances;

	// if expt supports 'getTestInstances' method, find their size
	static int numInstances (Experiment expt) {
		try {
			Method m = expt.getClass().getMethod ("getTestInstances");
			testInstances = (List)(m.invoke (expt));
		} catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
			testInstances = new ArrayList (); // empty instances
		}
		return testInstances.size ();
	}
}

class ConstraintsResultsModel extends DefaultTableModel {
	private Model model;
	private List<Double> results;
	private final Experiment expt;
	private final String[] headers = new String[] { "Constraint", "Fitness" };
	
	ConstraintsResultsModel (Experiment expt) {
		super (expt.getConstraints().size (), 2);
		this.expt = expt;
	}

	public void setModel (Model model) {
		this.model = model;
		results = expt.evaluate (model);
		fireTableRowsUpdated (0, expt.getConstraints().size () - 1);
	}

	public String getColumnName (int c) {
		return headers[c];
	}

	public Class getColumnClass (int c) {
		return (c == 0 ? String.class : double.class);
	}
	
	public Object getValueAt (int r, int c) {
		if (results == null) return "";

		if (c == 0) {
			return expt.getConstraints().get(r).toString();
		} else { // (c == 1)
			return expt.getConstraints().get(r).computeMatch (results);
		}
	}

	public boolean isCellEditable (int row, int col) {
		return false;
	}
}

