package info.peterlane.mdk.optimisation;

import java.util.List;

/**
 * All experiments must provide a list of constraints and theories 
 * and a way to evaluate a model.  Classes implementing this 
 * interface are used by the @link{info.peterlane.mdk.optimisation.gui.Shell} 
 * or optimisation algorithm to control the simulation experiment.
 *
 * @author Peter Lane
 */
public interface Experiment {

	/**
	 * Retrieve a list of the constraints.  These should be the constraints 
	 * for <em>evaluating</em> the model.
	 */
	List<Constraint> getConstraints ();

  /**
   * Return a list of the classes for the models implementing each theory.
   * If your model class is MyModel implementing 
   * {@link info.peterlane.mdk.optimisation.Model} then MyModel.class would be 
   * used in the list to refer to the class of models.
   */
  List<Class<? extends Model>> getTheories ();

  /**
   * Evaluate the provided model, returning a list of the responses 
   * of the model to each of the stimuli used for the constraints.
   * It is assumed the size of the list will be the same as the 
   * number of stimuli used in the constraints.  The model must be 
   * built/trained using the current settings, and then tested on 
   * the target stimuli.
   */
  List<Double> evaluate (Model model);
}
