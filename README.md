# Evolving Models #

Software for evolving parameter sets for computational models, ranking by 
fitness or non-dominated sorting of multiple evaluations.

There are two major components to this project:

1. Library of optimisation algorithms, including:
   - API for writing simulation experiments and running a suitable 
     optimisation algorithm; and
   - GUI environment, for experimenting with the models interactively.
2. Example models and how to use the library.


## Using the Library ##

Include the file 'evolvingmodels-library.jar' on your CLASSPATH or 
within your project files.  The javadoc documentation is included 
under the folder 'doc/api'.

## Running the Example ##

The folder 'example' contains a complete example of using the 
software, using the Five-Four Task and four different models.

The graphical demonstration program can be run by double-clicking 
on the file 'fivefourexperiment.jar', or using the 'rundemo' scripts 
as appropriate to your operating system.

The example Java programs show some typical uses of the library, 
and must be compiled with the library jar file and the fivefourexperiment 
jar file (for the models and experiment definitions) on the classpath.

## Further Information ##

A manual is provided in the 'doc' folder.

Code used in publication:

* P.C.R. Lane and F. Gobet, 'Evolving non-dominated parameter sets for computational models from multiple experiments', _Journal of Artificial General Intelligence_, 4:1-30, 2013

