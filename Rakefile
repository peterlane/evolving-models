# Rake tasks to assist with building/running the software
# Specific to Ubuntu Linux.

# Compile/Run/Bundle

directory 'bin'

desc 'compile library classes into bin folder'
task :compile => 'bin' do
	sh "rm -rf bin/*"
  Dir.chdir('src') do
    sh "javac -d ../bin -Xlint:unchecked `find -name \"*.java\"`"
  end
  sh "cp -r src/info/peterlane/mdk/optimisation/gui/icons bin/info/peterlane/mdk/optimisation/gui"
end

directory 'bindemo'

desc 'compile example classes into bin folder'
task :compile_demo => ['bindemo', :compile] do
	sh "rm -rf bindemo/*"
  sh "javac -d bindemo -Xlint:unchecked -cp example:bin example/fivefourexperiment/FiveFourExperimentDemo.java"
  sh "cp -r example/fivefourexperiment/models/icons bindemo/fivefourexperiment/models"
end

desc 'run demo from compiled code'
task :demo => :compile_demo do
  sh "java -cp bindemo:bin fivefourexperiment.FiveFourExperimentDemo"
end

desc 'convert library to jar file'
task :jarlib => :compile do
  sh 'jar -cf evolvingmodels-library.jar -C bin .'
end

desc 'convert demo to jar file'
task :jardemo => [:compile_demo] do 
  File.open("Manifest", "w") do |file|
    file.puts <<END
Main-Class: fivefourexperiment.FiveFourExperimentDemo
END
  end
  sh 'jar -cfm fivefourexperiment.jar Manifest -C bindemo . -C bin .'
  sh "rm Manifest"
end

# documentation

directory 'doc/api'
desc 'create API documentation'
task :api => 'doc/api' do
  sh "rm -rf doc/api/*"
  Dir.chdir('src') do
    sh "javadoc -d ../doc/api `find -name \"*.java\"`"
  end
end

desc 'build the manual'
task :manual => 'doc/manual' do
  Dir.chdir('doc/manual') do
    if !File.exist?('manual.pdf') or (File.stat('manual.txt').mtime > File.stat('manual.pdf').mtime)
      sh "asciidoc-bib -b ~/writing/papers/biblio.bib -s chicago-author-date manual.txt"
      sh "a2x -fpdf -darticle --dblatex-opts \"-P latex.output.revhistory=0\" manual-ref.txt"
      sh "mv manual-ref.pdf manual.pdf"
    end
  end
end

task :show_manual => 'manual' do
  sh "atril doc/manual/manual.pdf"
end

directory 'evolving-models'
directory 'release'

desc 'build a release'
task :finish => ['api', 'manual', 'release', 'evolving-models', 'jarlib', 'jardemo'] do
  sh "mv evolvingmodels-library.jar evolving-models"

  sh "mkdir evolving-models/doc"
  sh "cp -r doc/api evolving-models/doc"
  sh "cp doc/manual/manual.pdf evolving-models/doc"

  sh "mkdir evolving-models/example"
  sh "cp example/README.md evolving-models/example"
  File.open("example/README.md", "w") do |file|
    file.puts <<END
# Examples

The jar file 'fivefourexperiment' contains implementation of the data 
and models for the 5-4 experiment, and a gui application to experiment 
with the algorithms and datasets.  The jar file can be run (by double-clicking).

Java files in this 'examples' folder use the library and models in 
'fivefourexperiment' to illustrate direct use of the library as simple 
command-line programs to locate and retrieve optimised model sets.
To compile, you must include both the library and fivefourexperiment.jar 
on the classpath.
END
  end
  sh "cp example/*.java evolving-models/example"
  sh "mv fivefourexperiment.jar evolving-models/example"
  File.open("evolving-models/example/rundemo.bat", "w") do |file|
    file.puts <<END
    javaw -jar fivefourexperiment.jar
END
  end

  File.open("evolving-models/example/rundemo.sh", "w") do |file|
    file.puts <<END
    java -jar fivefourexperiment.jar
END
  end

  sh "cp README.md evolving-models"
  sh "cp LICENSE.txt evolving-models"

  sh "zip -r evolving-models.zip evolving-models"
  sh "mv evolving-models.zip release"
  sh "rm -rf evolving-models"
  sh "md5sum release/evolving-models.zip"
end
